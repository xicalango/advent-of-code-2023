
(include "include/list_util.lisp")

(define numbers (map to-string (list 0 1 2 3 4 5 6 7 8 9)))
(define is-number (lambda (s) 
    (not (== (index-of s numbers) ()))
))

(define find-first-last-number (lambda (l)
    (define ffln (lambda (l ef el)
        (if (endp l)
            (list ef el)
            (progn
                (define v (car l))
                (if (is-number v)
                    (progn
                        (define num (parse-int v))
                        (if (== ef 0)
                            (ffln (cdr l) num num)
                            (ffln (cdr l) ef num)
                        )
                    )
                    (ffln (cdr l) ef el)
                )
            )
        )
    ))
    (ffln l 0 0)
))

(if (== (length (cli-args)) 1)
    (define input-file (car (cli-args)))
    (define input-file "../data/day1/example.txt")
)

(define input (read-file input-file))

(define res-numbers (map 
    (lambda (line) 
        (define split (str-split line ""))

        (define res (find-first-last-number split))

        (define first-digit (car res))
        (define last-digit (car (cdr res)))

        (+ (* first-digit 10) last-digit)
    ) 
    (str-lines input)
))

(print (sum-all 0 res-numbers))
