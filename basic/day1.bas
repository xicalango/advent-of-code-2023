

Cls

ex_input_data$ = _CWD$ + "/../data/day1/input.txt"

Print ex_input_data$

Open ex_input_data$ For Input As #1

n = 0

While Not EOF(1)
    Input #1, line$

    f = -1
    l = -1

    For i = 1 To Len(line$)
        fc$ = Mid$(line$, i, 1)
        ec$ = Mid$(line$, Len(line$) - i + 1, 1)

        If f = -1 Then
            If is_digit(fc$) Then
                f = Val(fc$)
            End If
        End If
        If l = -1 Then
            If is_digit(ec$) Then
                l = Val(ec$)
            End If
        End If
        If f > -1 And l > -1 Then
            Exit For
        End If
    Next i
    Print f; l
    n = n + f * 10 + l
Wend

Print n


Function is_digit (c$)
    If Asc(c$) >= Asc("0") And Asc(c$) <= Asc("9") Then
        is_digit = 1
    Else
        is_digit = 0
    End If
End Function
