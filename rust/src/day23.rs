use std::{str::FromStr, collections::{HashMap, BinaryHeap}};

use anyhow::{Error, anyhow};
use num_integer::Integer;

use crate::utils::{direction::{Direction, TryWalkable, Walkable}, grid::{Grid2D, Grid2DAccess}, coor::UCoor};


#[derive(Debug, PartialEq, Eq)]
enum Tile {
    Wall,
    Floor,
    Slope(Direction),
}

impl TryFrom<char> for Tile {
    type Error = Error;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '#' => Ok(Tile::Wall),
            '.' => Ok(Tile::Floor),
            '>' => Ok(Tile::Slope(Direction::East)),
            '<' => Ok(Tile::Slope(Direction::West)),
            '^' => Ok(Tile::Slope(Direction::North)),
            'v' => Ok(Tile::Slope(Direction::South)),
            e => Err(anyhow!("invalid tile: {e}")),
        }
    }
}

impl Tile {

    fn slope_dir(&self) -> Option<&Direction> {
        if let Tile::Slope(dir) = self {
            Some(dir)
        } else {
            None
        }
    }

    fn is_walkable_from(&self, dir: &Direction) -> bool {
        match self {
            Tile::Floor => true,
            Tile::Slope(sd) if sd == dir => true,
            _ => false
        }
    }

    fn is_walkable(&self) -> bool {
        match self {
            Tile::Wall => false,
            _ => true,
        }
    }

}


#[derive(Debug)]
struct Map {
    grid: Grid2D<Tile>,
}

impl FromStr for Map {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let tiles: Result<Vec<Vec<Tile>>, _> = s.lines().map(|line| line.chars().map(|c| c.try_into()).collect()).collect();

        Ok(Map {
            grid: tiles?.into()
        })
    }
}

impl Map {

    fn find_start(&self) -> UCoor {
        self.grid.rows().next().and_then(|first_row| {
            let mut it = first_row;
            it.position(|t| t == &Tile::Floor).map(|p| (p, 0))
        }).unwrap()
    }

    fn find_end(&self) -> UCoor {
        self.grid.rows().last().and_then(|row| {
            let mut it = row;
            it.position(|t| t == &Tile::Floor).map(|p| (p, self.grid.height()-1))
        }).unwrap()
    }

    fn neighbors(&self, pos: &UCoor) -> Vec<UCoor> {
        if let Some(dir) = self.grid.get_coor(pos).and_then(|t| t.slope_dir()) {
            return vec![pos.walk(dir)];
        }

        Direction::all().iter()
            .filter_map(|dir| pos.try_walk(dir).map(|p| (dir, p)))
            .filter(|(dir, p)| self.grid.get_coor(p).is_some_and(|t| t.is_walkable_from(dir)))
            .map(|(_, p)| p)
            .collect()
    }

}

fn form_path(prev: &HashMap<UCoor, UCoor>, start: &UCoor) -> Vec<UCoor> {
    let mut cur = start;
    let mut path = vec![*start];

    while let Some(next) = prev.get(cur) {
        path.push(*next);
        cur = next;
    }

    path.reverse();
    path
}

struct LongestSlopedPath<'a> {
    map: &'a Map,
    cache: HashMap<(UCoor, UCoor), Vec<UCoor>>,
}

impl<'a> LongestSlopedPath<'a> {

    fn new(map: &'a Map) -> LongestSlopedPath {
        LongestSlopedPath { map, cache: Default::default() }
    }

    fn find_path(&mut self, start: UCoor, end: &UCoor) -> Vec<UCoor> {
        if let Some(path) = self.cache.get(&(start, *end)) {
            return path.clone();
        }

        let mut visit_next = Some(start);

        let mut path = vec![start.clone()];
        let mut prev = None;

        while let Some(cur) = visit_next.take() {

            if &cur == end {
                break;
            }

            let neighbors = self.map.neighbors(&cur);
            let neighbors: Vec<_> = neighbors.iter().filter(|c| !prev.is_some_and(|p| &&p == c)).collect();
            #[cfg(any(test, feature = "debug"))]
            println!("from {cur:?} to {neighbors:?}");

            let next = if neighbors.len() == 1 {
                neighbors.get(0).map(|c| **c)
            } else {
                let mut paths: Vec<Vec<UCoor>> = Vec::new();
                for &neighbor in neighbors.iter() {
                    paths.push(self.find_path(neighbor.to_owned(), end));
                }

                paths.iter().max_by_key(|v| v.len()).and_then(|v| v.get(0).map(|c| *c))
            };

            if let Some(next) = next{
                path.push(next);
                visit_next.replace(next);
            }

            prev.replace(cur);
        
        }

        #[cfg(any(test, feature = "debug"))]
        println!("{path:?}");

        self.cache.insert((start, *end), path.clone());

        path
    }

}

struct LongestPath<'a> {
    map: &'a Map,
    fwd_graph: HashMap<(UCoor, UCoor), usize>,
    rev_graph: HashMap<(UCoor, UCoor), usize>,
}

impl<'a> LongestPath<'a> {
    
    fn new(map: &'a Map) -> LongestPath {
        LongestPath {
            map,
            fwd_graph: HashMap::new(),
            rev_graph: HashMap::new(),
        }
    }

    fn compute_graph(&mut self, start: UCoor, end: &UCoor, mut force_next: Option<UCoor>) {

        println!("==> searching for {start:?} to {end:?}, forcing to {force_next:?}");

        let mut visit_next = Some((start, 0));

        let mut path = vec![start.clone()];
        let mut prev = None;

        while let Some((cur, dist)) = visit_next.take() {
            if &cur == end {
                println!("distance from {start:?} to {cur:?}: {dist}");
                self.fwd_graph.insert((start, cur), dist);
                self.rev_graph.insert((cur, start), dist);
                break;
            }

            if self.fwd_graph.contains_key(&(start, cur)) || self.rev_graph.contains_key(&(start, cur)) {
                continue;
            }

            let neighbors = self.map.neighbors(&cur);
            let neighbors: Vec<_> = neighbors.iter()
                .filter(|&c| force_next.map(|p| p == *c).unwrap_or(true))
                .filter(|&c| !prev.is_some_and(|p| p == *c))
                .collect();

            force_next = None;

            // println!("neighbors of {cur:?}: {neighbors:?}");

            if neighbors.len() == 1 {
                let next = *neighbors[0];
                path.push(next);
                visit_next.replace((next, dist + 1));
            } else {

                println!("distance from {start:?} to {cur:?}: {dist}");

                self.fwd_graph.insert((start, cur), dist);
                self.rev_graph.insert((cur, start), dist);

                for &neighbor in neighbors.iter() {
                    self.compute_graph(cur, end, Some(*neighbor));
                }
            }

            prev = Some(cur);

        }

        println!("==> done searching {start:?} to {end:?}");

    }

    fn to_undirected_graph(mut self) -> HashMap<(UCoor, UCoor), usize> {
        let mut result = HashMap::new();
        result.extend(self.fwd_graph.drain());
        result.extend(self.rev_graph.drain());
        result
    }

}

pub fn reverse_weights(graph: &mut HashMap<(UCoor, UCoor), usize>) {
    let max_weight = *graph.values().max().unwrap();
    graph.values_mut().for_each(|v| *v = max_weight - *v);
}

pub fn inverse_weights(graph: &mut HashMap<(UCoor, UCoor), usize>) {
    let lcm = graph.values().fold(1, |v1, v2| v1.lcm(v2));
    graph.values_mut().for_each(|v| *v = lcm / *v);
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct DijkstraState<'a> {
    coor: &'a UCoor,
    cost: usize,
}

impl<'a> Ord for DijkstraState<'a> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.cost.cmp(&self.cost).then_with(|| self.coor.cmp(&other.coor))
    }
}

impl<'a> PartialOrd for DijkstraState<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

struct Dijkstra<'a> {
    graph: &'a HashMap<(UCoor, UCoor), usize>,
}

impl<'a> Dijkstra<'a> {

    fn new(graph: &'a HashMap<(UCoor, UCoor), usize>) -> Dijkstra {
        Dijkstra { graph }
    }

    fn neighbors(&self, pos: &UCoor) -> Vec<(&UCoor, &usize)> {
        self.graph.iter().filter(|((s, _), _)| s == pos).map(|((_, k), d)| (k, d)).collect()
    }

    fn sssp(&self, start: &UCoor, end: &UCoor) -> Option<Vec<UCoor>> {
        let mut dist: HashMap<_, _> = self.graph.keys().map(|(k, _)| (k, usize::MAX)).collect();
        let mut prev = HashMap::new();

        let mut heap = BinaryHeap::new();

        dist.insert(start, 0);
        heap.push(DijkstraState { cost: 0, coor: start });

        while let Some(DijkstraState { coor, cost }) = heap.pop() {
            if coor == end {
                let mut path = vec![*coor];
                let mut cur = coor;
                while let Some(coor) = prev.get(cur) {
                    path.push(*coor);
                    cur = coor;
                }
                path.reverse();
                return Some(path);
            }

            if cost > dist[coor] {
                continue;
            }

            for (next_coor, next_cost) in self.neighbors(coor) {
                if prev.contains_key(&next_coor) {
                    continue;
                }

                let next = DijkstraState { coor: next_coor, cost: cost + *next_cost };

                if next.cost < dist[next.coor] {
                    heap.push(next);
                    dist.insert(next.coor, next.cost);
                    prev.insert(next_coor, *coor);
                    println!("can reach {next_coor:?} from {coor:?} with {next_cost}");
                }
            }

            println!("from {coor:?}/{cost} to {heap:?}");
        }


        None
    }


}


pub fn solution1(input: &str) -> usize {
    let map: Map = input.parse().unwrap();
    let mut lp = LongestSlopedPath::new(&map);
    let path = lp.find_path(map.find_start(), &map.find_end());
    println!("{}", lp.cache.len());
    path.len() - 1
}


#[cfg(test)]
mod test{

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day23/example.txt");
    const INPUT1: &'static str = include_str!("../../data/day23/my_example.txt");
    const INPUT2: &'static str = include_str!("../../data/day23/input.txt");

    #[test]
    fn test_map() {
        let map: Map = INPUT.parse().unwrap();
        let mut lp = LongestSlopedPath::new(&map);
        // println!("end: {:?}", map.find_end());
        let p = lp.find_path(map.find_start(), &map.find_end());
        println!("{}", p.len());
    }

    #[test]
    fn test_s1() {
        let s = solution1(INPUT);
        assert_eq!(s, 94);
    }

    #[test]
    fn test_dist_map() {
        let map: Map = INPUT2.parse().unwrap();
        let mut lp = LongestPath::new(&map);
        lp.compute_graph(map.find_start(), &map.find_end(), None);
        println!("{:?}", lp.fwd_graph);
        println!("{:?}", lp.fwd_graph.len());
        // let mut graph = lp.to_undirected_graph();
        // inverse_weights(&mut graph);
        // let d = Dijkstra::new(&graph);
        // let path = d.sssp(&map.find_start(), &map.find_end());
        // println!("{path:?}");
    }

}