use std::{collections::{HashMap, VecDeque}, str::FromStr, fmt::Display};
use anyhow::{Result, Error, anyhow};
use crate::utils::direction::Direction::{self, *};
use crate::utils::coor::UCoor;


#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tile {
    Floor,
    Pipe(Direction, Direction),
    Start,
}

use Tile::*;

impl TryFrom<char> for Tile {
    type Error = Error;

    fn try_from(value: char) -> Result<Self> {
        match value {
            '.' => Ok(Floor),
            '|' => Ok(Pipe(North, South)),
            '-' => Ok(Pipe(East, West)),
            'L' => Ok(Pipe(North, East)),
            'J' => Ok(Pipe(North, West)),
            '7' => Ok(Pipe(South, West)),
            'F' => Ok(Pipe(South, East)),
            'S' => Ok(Start),
            o => Err(anyhow!("invalid tile: {o}")),
        }
    }
}

impl FromStr for Tile {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        if s.len() != 1 {
            return Err(anyhow!("invalid length: {s}"));
        }

        match s.chars().nth(0) {
            Some(c) => c.try_into(),
            _ => Err(anyhow!("invalid tile: {s}")),
        }
    }
}

impl Display for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Floor => write!(f, "."),
            Pipe(North, South) => write!(f, "│"),
            Pipe(East, West) => write!(f, "─"),
            Pipe(North, East) => write!(f, "└"),
            Pipe(North, West) => write!(f, "┘"),
            Pipe(South, West) => write!(f, "┐"),
            Pipe(South, East) => write!(f, "┌"),
            Pipe(_, _) => panic!("invalid pipe"),
            Start => write!(f, "S"),
        } 
    }
}

impl Tile {
    fn connects_with(&self, other: &Tile) -> bool {
        match (self, other) {
            (Floor, _) | (_ , Floor) => false,
            (Start, _) | (_, Start) => true,
            (Pipe(ds1, ds2), Pipe(os1, os2)) => {
                ds1.connects_with(os1) || ds1.connects_with(os2) || ds2.connects_with(os1) || ds2.connects_with(os2)
            }
        }
    }
}

struct Map {
    map: HashMap<UCoor, Tile>,
    start_coor: UCoor,
}

impl FromStr for Map {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut map = HashMap::new();
        let mut start_coor = None;

        for (y, line) in s.lines().enumerate() {
            for (x, c) in line.trim_end().char_indices() {
                let tile = c.try_into()?;
                if tile == Start {
                    start_coor.replace((x, y));
                }
                map.insert((x, y), tile);
            }
        }

        let start_coor = start_coor.ok_or(anyhow!("no start"))?;

        if cfg!(feature = "debug") {
            println!("start: {start_coor:?}");
        }

        Ok(Map {
            map,
            start_coor,
        })
    }
}

impl Map {

    fn try_walk(&self, start: &UCoor, dir: &Direction) -> Option<UCoor> {
        let tile = &self.map[start];
        let next_c = dir.walk(start);
        let next_t = &self.map[&next_c];
        if tile.connects_with(next_t) {
            Some(next_c)
        } else {
            None
        }
    }

    fn try_walk_start(&self, start: &UCoor, dir: &Direction) -> Option<UCoor> {
        let tile = &self.map[start];
        assert_eq!(tile, &Start);

        let next_c = dir.walk(start);
        let next_t = &self.map[&next_c];

        match next_t {
            Pipe(d1, d2) if d1.connects_with(dir) || d2.connects_with(dir) => Some(next_c),
            _ => None,
        }

    }

    fn next_directions(&self, start: &UCoor) -> Vec<(UCoor, Direction)> {
        let tile = &self.map[start];
        match tile {
            Pipe(d1, d2) => {
                [d1, d2].into_iter().filter_map(|d| self.try_walk(start, d).map(|v| (v, d.clone()))).collect()
            },
            Start => Direction::all().into_iter().filter_map(|d| self.try_walk_start(start, &d).map(|v| (v, d))).collect(),
            Floor => vec![],
        }
    }

    fn bfs(&self) -> HashMap<UCoor, (usize, Vec<(UCoor, Direction)>)> {
        let mut queue: VecDeque<(UCoor, usize, Vec<(UCoor, Direction)>)> = vec![(self.start_coor.clone(), 0, vec![])].into();
        let mut result = HashMap::new();

        while let Some((cur, level, prev)) = queue.pop_front() {
            if cfg!(feature = "debug") {
                println!("visiting {cur:?} at {level} prev {prev:?}");
            }
            result.entry(cur.clone()).or_insert((level, vec![])).1.extend_from_slice(&prev);

            for (next, dir) in self.next_directions(&cur).into_iter().filter(|(c, _)| !result.contains_key(c)) {
                queue.push_back((next, level+1, vec![(cur.clone(), dir)]));
            }
        }

        result
    }

}

fn plot_map<D, DF: Fn(&UCoor, &D) -> char>(map: &HashMap<UCoor, D>, display_fn: DF) {
    let max_x = map.keys().map(|(x, _)| x).max().unwrap();
    let max_y = map.keys().map(|(_, y)| y).max().unwrap();

    for y in 0..=*max_y {
        for x in 0..=*max_x {
            let coor = &(x,y);
            match map.get(coor) {
                Some(v) => print!("{}", display_fn(coor, v)),
                None => print!(" "),
            }
        }
        println!();
    }
}

#[allow(unused)]
pub fn solution1(input: &str) -> usize {
    let map: Map = input.parse().unwrap();
    if cfg!(feature = "debug") {
        plot_map(&map.map, |_, t| format!("{t}").chars().nth(0).unwrap());
    }
    let bfs_result = map.bfs();
    if cfg!(feature = "debug") {
        plot_map(&bfs_result, |_, _| '.');
    }
    *bfs_result.values().map(|(level, _)| level).max().unwrap()
}

fn get_lr(dir: &Direction, left_dir: &Direction, inv_dir: &bool) -> (char, char) {
    if dir == left_dir {
        if !inv_dir {
            ('L', 'R')
        } else {
            ('R', 'L')
        }
    } else {
        if !inv_dir {
            ('R', 'L')
        } else {
            ('L', 'R')
        }
    }
}

pub fn solution12(input: &str) -> (usize, usize) {
    let map: Map = input.parse().unwrap();
    if cfg!(any(test, feature = "debug")) {
        plot_map(&map.map, |_, v| format!("{v}").chars().nth(0).unwrap());
    }
    let bfs_result = map.bfs();
    let max = *bfs_result.values().map(|(level, _)| level).max().unwrap();
    let (end_coor, (end_level, end_prev)) = bfs_result.iter().find(|(_, ref v)| v.0 == max).unwrap();

    assert_eq!(end_prev.len(), 2);

    if cfg!(any(test, feature = "debug")) {
        println!("end_coor: {end_coor:?} end_level: {end_level} end_prev: {end_prev:?}");

        plot_map(&bfs_result, |c, _| {
            if c == &map.start_coor {
                return 'S';
            } else if c == end_coor {
                return 'E';
            } else {
                return '.';
            }
        });
    }

    let mut lrp: HashMap<UCoor, char> = HashMap::new();
    for loop_coor in bfs_result.keys() {
        lrp.insert(loop_coor.clone(), '.');
    }

    let mut inv_dir = false;

    for prev in end_prev {
        if cfg!(any(test, feature = "debug")) {
            println!("path from {prev:?}");
        }

        let mut cur = Some(prev);
        while let Some((cur_coor, dir)) = cur {
            if cfg!(any(test, feature = "debug")) {
                println!("step: {cur:?} tile: {:?}", &map.map.get(cur_coor));
            }

            match &map.map[cur_coor] {
                Pipe(North, South) => {
                    let (dl, dr) = get_lr(dir, &South, &inv_dir);
                    lrp.entry(West.walk(cur_coor)).or_insert(dl);
                    lrp.entry(East.walk(cur_coor)).or_insert(dr);
                },
                Pipe(East, West) => {
                    let (dl, dr) = get_lr(dir, &West, &inv_dir);
                    lrp.entry(North.walk(cur_coor)).or_insert(dl);
                    lrp.entry(South.walk(cur_coor)).or_insert(dr);
                },
                Pipe(North, East) => {
                    let (dl, dr) = get_lr(dir, &North, &inv_dir);
                    lrp.entry(West.walk(cur_coor)).or_insert(dr);
                    lrp.entry(South.walk(cur_coor)).or_insert(dr);
                    lrp.entry(West.walk(&South.walk(cur_coor))).or_insert(dr);
                    lrp.entry(East.walk(&North.walk(cur_coor))).or_insert(dl);
                },
                Pipe(North, West) => {
                    let (dl, dr) = get_lr(dir, &West, &inv_dir);
                    lrp.entry(East.walk(cur_coor)).or_insert(dr);
                    lrp.entry(South.walk(cur_coor)).or_insert(dr);
                    lrp.entry(South.walk(&East.walk(cur_coor))).or_insert(dr);
                    lrp.entry(North.walk(&West.walk(cur_coor))).or_insert(dl);
                },
                Pipe(South, West) => {
                    let (dl, dr) = get_lr(dir, &South, &inv_dir);
                    lrp.entry(East.walk(cur_coor)).or_insert(dr);
                    lrp.entry(North.walk(cur_coor)).or_insert(dr);
                    lrp.entry(East.walk(&North.walk(cur_coor))).or_insert(dr);
                    lrp.entry(West.walk(&South.walk(cur_coor))).or_insert(dl);
                },
                Pipe(South, East) => {
                    let (dl, dr) = get_lr(dir, &East, &inv_dir);
                    lrp.entry(West.walk(cur_coor)).or_insert(dr);
                    lrp.entry(North.walk(cur_coor)).or_insert(dr);
                    lrp.entry(West.walk(&North.walk(cur_coor))).or_insert(dr);
                    lrp.entry(East.walk(&South.walk(cur_coor))).or_insert(dl);
                },
                Start => {},
                _ => panic!(),
            }

            let next = bfs_result.get(cur_coor).and_then(|v| v.1.get(0));

            if cfg!(any(test, feature = "debug")) {
                plot_map(&lrp, |c, v| {
                    if c == &map.start_coor {
                        return 'S';
                    } else if c == end_coor {
                        return 'E';
                    } else if c == cur_coor { 
                        return '*';
                    } else if next.is_some_and(|(nc, _)| c == nc) {
                        return '#';
                    } else {
                        return *v;
                    }
                });
            }

            cur = next;
        }

        inv_dir = true;
    }

    if cfg!(any(test, feature = "debug", feature = "pretty")) {
        plot_map(&lrp, |c, v| {
            if c == &map.start_coor {
                return 'S';
            } else if c == end_coor {
                return 'E';
            } else {
                return *v;
            }
        });
    }


    let max_x = map.map.keys().map(|(x, _)| x).max().unwrap();
    let max_y = map.map.keys().map(|(_, y)| y).max().unwrap();

    let mut counter = 0;
    let mut inside = false;

    for y in 0..=*max_y {
        for x in 0..=*max_x {
            match lrp.get(&(x, y)) {
                Some('R') => {
                    counter += 1;
                    inside = true;
                }
                None => counter += inside as usize,
                _ => inside = false,
            }
        }
    }


    (max, counter)

}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT1: &'static str = include_str!("../../data/day10/example1.txt");
    const INPUT2: &'static str = include_str!("../../data/day10/example2.txt");
    const INPUT3: &'static str = include_str!("../../data/day10/example3.txt");
    const INPUT4: &'static str = include_str!("../../data/day10/example4.txt");

    #[test]
    fn test_s1_e1() {
        let s = solution1(INPUT1);
        assert_eq!(s, 4);
    }

    #[test]
    fn test_s1_e2() {
        let s = solution1(INPUT2);
        assert_eq!(s, 8);
    }


    #[test]
    fn test_s2_e1() {
        let s = solution12(INPUT1);
        println!("{s:?}");
    }

    #[test]
    fn test_s2_e2() {
        let s = solution12(INPUT2);
        println!("{s:?}");
    }

    #[test]
    fn test_s2_e3() {
        let s = solution12(INPUT3);
        println!("{s:?}");
        // assert_eq!(s, 4);
    }
    
    #[test]
    fn test_s2_e4() {
        let s = solution12(INPUT4);
        println!("{s:?}");
        assert_eq!(s.1, 8);
    }

}
