use std::str::FromStr;
use std::convert::TryInto;

use anyhow::{Result, Error, anyhow};

#[derive(Debug, PartialEq, Eq)]
enum Card {
    Ace,
    King,
    Queen,
    Jack,
    Number(u8),
}

impl Card {
    fn rank<const HAS_JOKER: bool>(&self) -> u8 {
        if HAS_JOKER {
            match self {
                Card::Ace => 14,
                Card::King => 13,
                Card::Queen => 12,
                Card::Jack => 1,
                Card::Number(i) => *i,
            }
        } else {
            match self {
                Card::Ace => 14,
                Card::King => 13,
                Card::Queen => 12,
                Card::Jack => 11,
                Card::Number(i) => *i,
            }
        }
    }

    pub fn cmp<const HAS_JOKER: bool>(&self, other: &Card) -> std::cmp::Ordering {
        self.rank::<HAS_JOKER>().cmp(&other.rank::<HAS_JOKER>())
    }
}

impl TryFrom<char> for Card {
    type Error = Error;

    fn try_from(value: char) -> Result<Self> {
         match value {
           'A' => Ok(Card::Ace),
           'K' => Ok(Card::King),
           'Q' => Ok(Card::Queen),
           'J' => Ok(Card::Jack),
           'T' => Ok(Card::Number(10)),
           n if n.is_ascii_digit() => Ok(Card::Number(n.to_digit(10).unwrap() as u8)),
           e => Err(anyhow!("invalid char: {e}")),
         }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

#[derive(Debug, PartialEq, Eq)]
struct Hand<const HAS_JOKER: bool> {
    cards: [Card; 5],
    hand_type: HandType,
}

impl<const HAS_JOKER: bool> Hand<HAS_JOKER> {

    pub fn from_cards(cards: [Card; 5]) -> Hand<HAS_JOKER> {
        let hand_type = Self::hand_type(&cards);

        Hand {
            cards,
            hand_type,
        } 
    }

    fn hand_type(hand: &[Card; 5]) -> HandType {
        let mut counter = [0; 14];

        for card in hand {
            counter[(card.rank::<HAS_JOKER>() - 1) as usize] += 1;
        }

        let num_jokers = counter[0];
        counter[0] = 0;

        counter.sort();
        counter.reverse();

        match counter[0] + num_jokers {
            5 => HandType::FiveOfAKind,
            4 => HandType::FourOfAKind,
            3 => {
                if counter[1] == 2 {
                    HandType::FullHouse
                } else {
                    HandType::ThreeOfAKind
                }
            },
            2 => {
                if counter[1] == 2 {
                    HandType::TwoPair
                } else {
                    HandType::OnePair
                }
            },
            _ => HandType::HighCard,
        }

    }

    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        use std::cmp::Ordering::Equal;

        match self.hand_type.cmp(&other.hand_type) {
            Equal => {
                for (c1, c2) in self.cards.iter().zip(other.cards.iter()) {
                    match c1.cmp::<HAS_JOKER>(c2) {
                        Equal => {},
                        other => {
                            return other;
                        },
                    }
                }
                return Equal;
            },
            other => {
                return other;
            },
        };

    }

}

impl<const HAS_JOKER: bool> From<[Card; 5]> for Hand<HAS_JOKER> {
    fn from(value: [Card; 5]) -> Self {
        Self::from_cards(value)
    }
}

impl<const HAS_JOKER: bool> FromStr for Hand<HAS_JOKER> {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
         let cards: Result<Vec<Card>> = s.chars().map(|c| c.try_into()).collect();
         let cards: [Card; 5] = cards?.try_into().map_err(|h| anyhow!("invalid hand {h:?}"))?;
         Ok(cards.into())
    }
}

fn parse_input<const HAS_JOKER: bool>(input: &str) -> Vec<(Hand<HAS_JOKER>, usize)> {
    input.lines().map(|line| {
        let (hand, bet) = line.split_once(' ').unwrap();
        let hand: Hand<HAS_JOKER> = hand.parse().unwrap();
        let bet: usize = bet.parse().unwrap();
        (hand, bet)
    }).collect()
}

pub fn solution<const HAS_JOKER: bool>(input: &str) -> usize {
    let mut games: Vec<(Hand<HAS_JOKER>, usize)> = parse_input(input);
    games.sort_by(|(h1, _), (h2, _)| h1.cmp(h2));
    games.into_iter().map(|(_, b)| b).enumerate().map(|(r, b)| (r+1) * b).sum()
}

pub fn solution1(input: &str) -> usize {
    solution::<false>(input)
}

pub fn solution2(input: &str) -> usize {
    solution::<true>(input)
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day7/example.txt");

    #[test]
    fn test() {
        let s = solution1(INPUT);
        println!("{s}");
        assert_eq!(s, 6440);
    }

    #[test]
    fn test2() {
        let s = solution2(INPUT);
        println!("{s}");
        assert_eq!(s, 5905);
    }

    #[test]
    fn test_cmp_fail() {
        let h1: Hand<false> = "QQQJA".parse().unwrap();
        let h2: Hand<false> = "T55J5".parse().unwrap();
        let c = h1.cmp(&h2);
        println!("{c:?}");

        let mut v = vec![h1, h2];
        v.sort_by(|h1, h2| h1.cmp(h2));
        println!("{v:?}");
    }

}
