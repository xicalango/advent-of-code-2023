
mod utils;

mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day18;
mod day19;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;

mod solution;
mod bench;

use bench::Bench;
use solution::{Solution, Day, SolutionReporter};
use utils::day_enabled;

macro_rules! run_day {
    ($bench:ident, $day:literal) => {
        if day_enabled($day) {
            $bench.run::<Day<$day>>();
        } 
    };
}

macro_rules! run_days {
    ($bench: ident, $( $day:literal ),*) => {
        $(
            run_day!($bench, $day);
        )*
    }
}

macro_rules! input_data {
    ($day:literal) => {
       include_str!(concat!("../../data/day", stringify!($day), "/input.txt"))
    };
}

fn main() {

    let mut bench = Bench::new();

    run_days!(bench, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23, 24);

    bench.print_times();
    println!();
    bench.print_total_time();
    println!();
    bench.print_slowest_days::<5>();
}

impl Solution for Day<1> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(1);
        r.run_all(data, day1::solution1, day1::solution2);
    }
}

impl Solution for Day<2> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(2);
        r.run(|| day2::solution1::<12,13,14>(data).unwrap());
        r.run(|| day2::solution2(data).unwrap());
    }
}

impl Solution for Day<3> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(3);
        let manual: day3::Manual = r.preprocess(|| data.parse().unwrap());
        r.run(|| manual.combined_solutions())
    }
}

impl Solution for Day<4> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(4);
        r.run_all(data, day4::solution1, day4::solution2);
    }
}

impl Solution for Day<5> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(5);
        r.run(|| day5::solution1(data).unwrap());
        r.run(|| day5::solution2(data).unwrap());
    }
}

impl Solution for Day<6> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(6);
        r.run_all(data, day6::solution1, day6::solution2);
    }
}

impl Solution for Day<7> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(7);
        r.run_all(data, day7::solution1, day7::solution2);
    }
}

impl Solution for Day<8> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(8);
        r.run_all(data, day8::solution1, day8::solution2);
    }
}

impl Solution for Day<9> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(9);
        r.run_all(data, day9::solution1, day9::solution2);
    }
}

impl Solution for Day<10> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(10);
        r.run_one(data, day10::solution12);
    }
}

impl Solution for Day<11> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(11);
        let map: day11::Map = r.preprocess(|| data.parse().unwrap());
        r.run(|| map.solution::<2>());
        r.run(|| map.solution::<1000000>());
    }
}

impl Solution for Day<12> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(12);
        r.run_all(data, day12::solution1, day12::solution2);
    }
}

impl Solution for Day<13> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(13);
        r.run_all(data, day13::solution1, day13::solution2);
    }
}

impl Solution for Day<14> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(14);
        r.run_all(data, day14::solution1, day14::solution2);
    }
}

impl Solution for Day<15> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(15);
        r.run_all(data, day15::solution1, day15::solution2);
    }
}

impl Solution for Day<16> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(16);
        r.run_all(data, day16::solution1, day16::solution2);
    }
}

impl Solution for Day<18> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(18);
        r.run_all(data, day18::solution1, day18::solution2);
    }
}

impl Solution for Day<19> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(19);
        r.run_all(data, day19::solution1, day19::solution2);
    }
}

impl Solution for Day<20> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(20);
        r.run_one(data, day20::solution1);
    }
}

impl Solution for Day<21> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(21);
        r.run(|| day21::solution1::<64>(data));
    }
}

impl Solution for Day<22> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(22);
        r.run_one(data, day22::solution1);
    }
}

impl Solution for Day<23> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(23);
        r.run_one(data, day23::solution1);
    }
}

impl Solution for Day<24> {
    fn main(r: &mut impl SolutionReporter) {
        let data = input_data!(24);
        r.run(|| day24::solution1::<200000000000000, 400000000000000>(data));
    }
}


