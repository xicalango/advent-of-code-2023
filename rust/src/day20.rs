
use std::{collections::{HashMap, VecDeque}, str::FromStr};
use std::hash::Hash;

use anyhow::{Error, anyhow, Result};


#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
enum State {
    Low,
    High,
}

impl Default for State {
    fn default() -> Self {
        State::Low
    }
}

impl State {
    fn toggle(&self) -> State {
        match self {
            State::Low => Self::High,
            State::High => Self::Low,
        }
    }

    fn toggle_self(&mut self) -> State {
        let cur = self.clone();
        *self = self.toggle();
        cur
    }

    fn is_high(&self) -> bool {
        match self {
            State::Low => false,
            State::High => true,
        }
    }

    fn is_low(&self) -> bool {
        !self.is_high()
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum ModuleFunction {
    Broadcast,
    FlipFlop(State),
    Conjunction(HashMap<String, State>),
}

impl TryFrom<char> for ModuleFunction {
    type Error = Error;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        let v = match value {
            'b' => Self::Broadcast,
            '%' => Self::FlipFlop(State::Low),
            '&' => Self::Conjunction(HashMap::new()),
            e => return Err(anyhow!("invalid: {e}")),
        };
        Ok(v)
    }
}

impl ModuleFunction {

    fn is_low(&self) -> bool {
        match self {
            ModuleFunction::Broadcast => true,
            ModuleFunction::FlipFlop(State::Low) => true,
            ModuleFunction::FlipFlop(State::High) => false,
            ModuleFunction::Conjunction(inps) => inps.values().all(|i| i.is_low()),
        }
    }

}

impl Hash for ModuleFunction {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        core::mem::discriminant(self).hash(state);

        match self {
            ModuleFunction::FlipFlop(s) => s.hash(state),
            ModuleFunction::Conjunction(v) => v.values().for_each(|v| v.hash(state)),
            _ => {},
        }

    }
}

#[derive(Debug, Clone)]
struct Module {
    name: String,
    output: Vec<String>,
    function: ModuleFunction,
}

impl FromStr for Module {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (id, dests) = s.split_once(" -> ").ok_or(anyhow!("invalid: {s}"))?;
        let module_type = id.chars().next().ok_or(anyhow!("invalid: {s}"))?;
        let function = module_type.try_into()?;

        let name = if let ModuleFunction::Broadcast = function {
            id.to_owned()
        } else {
            id[1..].to_owned()
        };

        let output = dests.split(", ").map(|s| s.to_owned()).collect();

        Ok(Module { name, output, function })
    }
}

impl Module {

    fn create_broadcast(&self, state: State) -> Vec<(&str, State)> {
        self.output.iter().map(|o| (o.as_str(), state)).collect()
    }

    fn receive(&mut self, from_module: &str, state: State) -> Vec<(&str, State)> {
        
        match &mut self.function {
            ModuleFunction::Broadcast => self.create_broadcast(state),
            ModuleFunction::FlipFlop(s) => {
                match state {
                    State::Low => {
                        s.toggle_self();
                        let state = s.clone();
                        self.create_broadcast(state)
                    },
                    State::High => Vec::new(),
                }
            },
            ModuleFunction::Conjunction(input_states) => {
                *input_states.get_mut(from_module).unwrap() = state;
                let signal = if input_states.values().all(|v| v.is_high()) {
                    State::Low
                } else {
                    State::High
                };
                self.create_broadcast(signal)
            },
        }

    }

}


#[derive(Debug)]
struct Modules(HashMap<String, Module>);

impl FromStr for Modules {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let modules : Result<_>  = s.lines().map(|v| v.parse::<Module>())
                                            .map(|r| r.map(|m| (m.name.clone(), m)))
                                            .collect();

        let mut input_mapping: HashMap<String, Vec<String>> = HashMap::new();

        let mut modules: HashMap<String, Module> = modules?;

        for module in modules.values() {
            for output in &module.output {
                if let Some(dest_module) = modules.get(output) {
                    if let ModuleFunction::Conjunction(_) = &dest_module.function {
                        input_mapping.entry(dest_module.name.clone()).or_insert_with(Vec::new).push(module.name.clone());
                    }
                }
            }
        }

        for (module, inputs) in input_mapping.into_iter() {
            if let ModuleFunction::Conjunction(module_inputs) = &mut modules.get_mut(&module).unwrap().function {
                for input in inputs {
                    module_inputs.insert(input, State::Low);
                }
            }
        }

        Ok(Modules(modules))
    }

}

impl Modules {

    fn all_low(&self) -> bool {
        let Modules(modules) = self;
        modules.values().all(|m| m.function.is_low())
    }

    fn simulate(&mut self) -> usize {
        let mut low_pulses = 0;
        let mut high_pulses = 0;

        #[cfg(any(test, feature = "debug"))]
        println!("{:#?}", self.0);


        for _i in 0..1000 {

            let mut work_queue: VecDeque<(String, String, State)> = vec![("button".to_owned(), "broadcaster".to_owned(), State::Low)].into();

            while let Some((src, dst, state)) = work_queue.pop_front() {
                match state {
                    State::Low => low_pulses += 1,
                    State::High => high_pulses += 1,
                }

                #[cfg(any(feature = "debug"))]
                println!("({src})-[{state:?}]->({dst})");
                if let Some(module) = self.0.get_mut(&dst) {
                    #[cfg(feature = "debug")]
                    println!("sending {state:?} to {module:?}");

                    for (next, state) in module.receive(&src, state) {
                        work_queue.push_back((dst.clone(), next.to_owned(), state));
                    }

                } else {
                    #[cfg(feature = "debug")]
                    println!("... went nowhere")
                }

                #[cfg(feature = "debug")]
                {
                    println!("work queue: {work_queue:?}");
                    println!();
                }
            }

            #[cfg(any(feature = "debug"))]
            println!("iteration {i}: low: {low_pulses} high: {high_pulses}");

        }

        #[cfg(any(test, feature = "debug"))]
        println!("end low: {low_pulses} high: {high_pulses}");

        low_pulses * high_pulses
    }

    fn simulate_rx(&mut self) -> usize {

        let mut i = 0usize;

        loop {

            i += 1;

            let mut work_queue: VecDeque<(String, String, State)> = vec![("button".to_owned(), "broadcaster".to_owned(), State::Low)].into();

            while let Some((src, dst, state)) = work_queue.pop_front() {
                if let Some(module) = self.0.get_mut(&dst) {
                    for (next, state) in module.receive(&src, state) {
                        work_queue.push_back((dst.clone(), next.to_owned(), state));
                    }
                }
            }

            let c = work_queue.iter().filter(|(_, d, s)| d == "rx" && s.is_low()).count();
            if c > 0 {
                println!("{work_queue:?}");
            }

        }

    }

}

pub fn solution1(input: &str) -> usize {
    let mut modules: Modules = input.parse().unwrap();
    modules.simulate()
}

pub fn solution2(input: &str) -> usize {
    let mut modules: Modules = input.parse().unwrap();
    modules.simulate_rx()
}

pub fn to_graph_viz(input: &str) {
    let modules: Modules = input.parse().unwrap();

    println!("digraph {{");

    for module in modules.0.values() {
        match module.function {
            ModuleFunction::Broadcast => println!("{} [shape=box];", &module.name),
            ModuleFunction::FlipFlop(_) => println!("{} [shape=doublecircle];", &module.name),
            ModuleFunction::Conjunction(_) => println!("{} [shape=diamond];", &module.name),
        }

        for output in &module.output {
            println!("{} -> {output};", &module.name);
        }
    }

    println!("}}");
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT1: &'static str = include_str!("../../data/day20/example1.txt");
    const INPUT2: &'static str = include_str!("../../data/day20/example2.txt");
    const INPUT3: &'static str = include_str!("../../data/day20/input.txt");

    #[test]
    fn test_simulate_1() {
        let mut modules: Modules = INPUT1.parse().unwrap();
        let s = modules.simulate();
        assert_eq!(s, 32000000);
    }

    #[test]
    fn test_simulate_2() {
        let mut modules: Modules = INPUT2.parse().unwrap();
        let s = modules.simulate();
        assert_eq!(s, 11687500);
    }

    #[test]
    fn graphviz() {
        to_graph_viz(INPUT3);
    }

}