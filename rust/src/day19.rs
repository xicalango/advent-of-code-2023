use std::{str::FromStr, collections::HashMap, ops::RangeInclusive};

use anyhow::{Error, anyhow};

use crate::utils::ranges::RangeLength;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Category {
    X,
    M,
    A,
    S,
}

impl TryFrom<char> for Category {
    type Error = Error;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        let c = match value {
            'x' => Category::X,
            'm' => Category::M,
            'a' => Category::A,
            's' => Category::S,
            e => return Err(anyhow!("invalid: {e}")),
        };

        Ok(c)
    }
}

impl From<Category> for usize {
    fn from(value: Category) -> Self {
        (&value).into()
    }
}

impl From<&Category> for usize {
    fn from(value: &Category) -> Self {
        match value {
            Category::X => 0,
            Category::M => 1,
            Category::A => 2,
            Category::S => 3,
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
enum Op {
    Lt,
    Gt,
}

impl TryFrom<char> for Op {
    type Error = Error;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        let op = match value {
            '<' => Op::Lt,
            '>' => Op::Gt,
            e => return Err(anyhow!("invalid: {e}")),
        };
        Ok(op)
    }
}

impl Op {

    fn eval<T: Ord>(&self, v1: T, v2: T) -> bool {
        match self {
            Op::Lt => v1 < v2,
            Op::Gt => v1 > v2,
        }
    }

}

#[derive(Debug, Clone)]
enum Destination {
    Workflow(String),
    Accepted,
    Rejected,
}

impl FromStr for Destination {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(c) = s.chars().next() {
            match c {
                'R' => return Ok(Destination::Rejected),
                'A' => return Ok(Destination::Accepted),
                _ => {},
            }
        }

        Ok(Destination::Workflow(s.to_string()))
    }
}

#[derive(Debug)]
enum Rule {
    Expression {
        category: Category,
        op: Op,
        value: usize,
        destination: Destination,
    },
    Default(Destination),
}

impl FromStr for Rule {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let rule = if s.contains(&['<','>']) {
            let (expr, dest) = s.split_once(':').ok_or(anyhow!("invalid: {s}"))?;

            let category = expr.chars().nth(0).unwrap().try_into()?;
            let op = expr.chars().nth(1).unwrap().try_into()?;
            let value = expr[2..].parse()?;
            let destination = dest.parse()?;

            Rule::Expression { category, op, value, destination }
        } else {
            Rule::Default(s.parse()?)
        };

        Ok(rule)
    }
}

impl Rule {

    fn eval(&self, specs: &Specs) -> Option<&Destination> {
        match self {
            Rule::Expression { category, op, value, destination } => {
                let v = specs.get(category);
                if op.eval(v, *value) {
                    Some(destination)
                } else {
                    None
                }
            },
            Rule::Default(dest) => Some(dest),
        }
    }

}

#[derive(Debug)]
struct Workflow {
    name: String,
    rules: Vec<Rule>,
}

impl FromStr for Workflow {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (name, wf) = s.split_once('{').ok_or(anyhow!("invalid: {s}"))?;

        let wf = &wf[..wf.len()-1];

        let rules: Result<Vec<_>, Error> = wf.split(',').map(|v| v.parse()).collect();

        Ok(Workflow { name: name.to_owned(), rules: rules? })
    }
}

#[derive(Debug)]
struct Spec {
    category: Category,
    value: usize,
}

impl FromStr for Spec {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let category = s.chars().next().ok_or(anyhow!("invalid: {s}"))?.try_into()?;
        let value = s[2..].parse()?;

        Ok(Spec { category, value })
    }
}

#[derive(Debug)]
struct Specs([usize; 4]);

impl FromStr for Specs {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = &s[1..s.len()-1];

        let mut result: [usize; 4] = Default::default();

        for spec in s.split(',').map(|v| v.parse::<Spec>()) {
            let spec = spec?;
            let i: usize = spec.category.into();
            result[i] = spec.value;
        }

        Ok(Specs(result))
    }
}

impl Specs {

    fn get(&self, cat: &Category) -> usize {
        let Specs(specs) = self;
        let i: usize = cat.into();
        specs[i]
    }

    fn score(&self) -> usize {
        let Specs(specs) = self;
        specs.iter().sum() 
    }

}

#[derive(Debug, Clone)]
struct SpecsRange([RangeInclusive<usize>; 4]);

impl Default for SpecsRange {
    fn default() -> Self {
        Self([1..=4000, 1..=4000, 1..=4000, 1..=4000])
    }
}

impl SpecsRange {

    fn get(&self, cat: &Category) -> &RangeInclusive<usize> {
        let SpecsRange(specs) = self;
        let i: usize = cat.into();
        &specs[i]
    }

    fn put(&mut self, cat: &Category, range: RangeInclusive<usize>) {
        let SpecsRange(specs) = self;
        let i: usize = cat.into();
        specs[i] = range;
    }

    fn split_on(&self, rules: &Vec<Rule>) -> Vec<(SpecsRange, Destination)> {
        let mut cur = self.clone();
        let mut result = Vec::new();

        for rule in rules {
            match rule {
                Rule::Expression { category, op, value, destination } => {
                    let _i: usize = (*category).into();
                    let range = cur.get(category);

                    let (range_in, range_out) = match op {
                        Op::Lt => (*range.start()..=value-1, *value..=*range.end()),
                        Op::Gt => (value+1..=*range.end(), *range.start()..=*value),
                    };

                    let mut next  = cur.clone();
                    next.put(category, range_in);
                    result.push((next, destination.to_owned()));

                    cur.put(category, range_out);
                },
                Rule::Default(dest) => result.push((cur.clone(), dest.to_owned())),
            }
        }

        result
    }

    fn score(&self) -> usize {
        let SpecsRange(specs) = self;
        specs.iter().map(|s| s.len()).product()
    }

}

#[derive(Debug)]
struct WorkflowEvaluator<'a> {
    workflows: &'a HashMap<String, Vec<Rule>>,
    specs: &'a Specs,
    cur: String,
}

impl<'a> WorkflowEvaluator<'a> {

    fn new(workflows: &'a HashMap<String, Vec<Rule>>, specs: &'a Specs) -> WorkflowEvaluator<'a> {
        WorkflowEvaluator { workflows, specs, cur: "in".to_owned() }
    }

    fn eval(&mut self) -> bool {
        loop {
            let rules = &self.workflows[&self.cur];
            for rule in rules {
                match rule.eval(&self.specs) {
                    Some(Destination::Accepted) => return true,
                    Some(Destination::Rejected) => return false,
                    Some(Destination::Workflow(wf)) => {
                        self.cur = wf.clone();
                        break;
                    },
                    None => {},
                }
            }
        }
    }

}

pub fn solution1(input: &str) -> usize {
    let mut workflows: HashMap<String, Vec<Rule>> = HashMap::new();
    let mut read_specs = false;

    let mut accepted_specs = Vec::new();

    for line in input.lines().map(|l| l.trim_end()) {
        if line.is_empty() {
            read_specs = true;
            continue;
        }

        match read_specs {
            false => {
                let Workflow{name, rules} = line.parse().unwrap();
                workflows.insert(name, rules);
            },
            true => {
                let specs: Specs = line.parse().unwrap();
                if WorkflowEvaluator::new(&workflows, &specs).eval() {
                    accepted_specs.push(specs);
                }
            }
        }
    }

    accepted_specs.iter().map(|s| s.score()).sum()
}

pub fn solution2(input: &str) -> usize {
    let mut workflows: HashMap<String, Vec<Rule>> = HashMap::new();

    let mut accepted_specs = Vec::new();

    for line in input.lines().map(|l| l.trim_end()) {
        if line.is_empty() {
            let mut specs = vec![(SpecsRange::default(), Destination::Workflow("in".to_owned()))];

            while let Some((range, dest)) = specs.pop() {
                #[cfg(feature = "debug")]
                println!("{range:?} {dest:?}");
                match dest {
                    Destination::Workflow(wf) => {
                        let wf = &workflows[&wf];
                        let mut splits = range.split_on(wf);
                        specs.append(&mut splits);
                    },
                    Destination::Accepted => accepted_specs.push(range),
                    Destination::Rejected => {},
                }
                #[cfg(feature = "debug")]
                println!("{specs:?}");
            }
            break;
        }

        let Workflow{name, rules} = line.parse().unwrap();
        workflows.insert(name, rules);
    }

    #[cfg(feature = "debug")]
    println!("{accepted_specs:#?}");

    accepted_specs.iter().map(|s| s.score()).sum()
}


#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day19/example.txt");
    const SIMPLE: &'static str = include_str!("../../data/day19/simple.txt");

    #[test]
    fn test_s1() {
        let s = solution1(INPUT);
        assert_eq!(s, 19114);
    }

    #[test]
    fn test_s2() {
        let s = solution2(INPUT);
        assert_eq!(s, 167409079868000);
    }

    #[test]
    fn test_simple() {
        let s = solution2(SIMPLE);
        println!("{s}");
    }

    #[test]
    fn test_split() {
        let workflow: Workflow = "in{s<1351:px,qqz}".parse().unwrap();
        let range = SpecsRange::default();
        let split = range.split_on(&workflow.rules);
        println!("{split:?}");
    }

}
