use std::hash::{Hash, Hasher};

#[derive(Debug, Default)]
struct HolidayHasher(u64);

impl Hasher for HolidayHasher {
    fn finish(&self) -> u64 {
        self.0
    }

    fn write(&mut self, bytes: &[u8]) {
        let HolidayHasher(cur) = self;
        for b in bytes {
            *cur += *b as u64;
            *cur *= 17;
            *cur %= 256;
        }
    }
}

fn hash(s: &str) -> usize {
    let mut hasher = HolidayHasher::default();
    s.as_bytes().iter().for_each(|b| b.hash(&mut hasher));
    hasher.finish() as usize
}

pub fn solution1(input: &str) -> usize {
    input.split(',')
        .map(|s| hash(s))
        .sum()
}

pub fn solution2(input: &str) -> usize {

    let mut boxes: Vec<Vec<(&str, usize)>> = Vec::new();
    boxes.resize_with(256, Vec::new);

    for instr in input.split(',') {

        if let Some((label, _)) = instr.split_once('-') {
            let cur_box = boxes.get_mut(hash(label)).unwrap();
            if let Some(i) = cur_box.iter().position(|(v, _)| v == &label) {
                cur_box.remove(i);
            }
        } else if let Some((label, len)) = instr.split_once('=') {
            let cur_box = boxes.get_mut(hash(label)).unwrap();
            let len: usize = len.parse().unwrap();

            if let Some((_, v)) = cur_box.iter_mut().find(|(v, _)| v == &label) {
                *v = len;
            } else {
                cur_box.push((label, len));
            }
        } else {
            panic!("invalid instruction: {instr}");
        }

        if cfg!(any(test, feature = "debug")) {
            boxes.iter().enumerate().for_each(|(i, b)| {
                if !b.is_empty() {
                    println!("box {}: {b:?}", i+1);
                }
            });
            println!();
        }
    }


    boxes.iter().enumerate().map(|(box_id, b)| {
        b.iter().enumerate().map(|(slot_id, (_, v))| {
            (box_id + 1) * (slot_id + 1) * v
        }).sum::<usize>()
    }).sum()
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day15/example.txt");

    #[test]
    fn test_hash() {
        let s = "HASH";
        let mut hh = HolidayHasher::default();
        for c in s.as_bytes() {
            c.hash(&mut hh)
        }
        assert_eq!(hh.finish(), 52);
    }

    #[test]
    fn test_solution1() {
        let s = solution1(INPUT);
        assert_eq!(s, 1320);
    }

    #[test]
    fn test_solution2() {
        let s = solution2(INPUT);
        assert_eq!(s, 145);
    }

}
