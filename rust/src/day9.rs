
pub fn solution1(input: &str) -> usize {

    let mut result = 0;

    for line in input.lines() {

        let nums: Vec<i64> = line.split_ascii_whitespace().map(|v| v.parse().unwrap()).collect();
        let mut diffs: Vec<Vec<i64>> = vec![nums];

        while diffs.last().unwrap().iter().any(|v| v != &0) {
            let next: Vec<i64> = diffs.last().unwrap().windows(2).map(|v| v[1] - v[0]).collect();
            diffs.push(next);
        }

        diffs.reverse();

        for i in 0..diffs.len()-1 {
            let cur = *diffs[i].last().unwrap();
            let next = &mut diffs[i+1];
            next.push(next.last().unwrap() + cur);
        }
        result += diffs.last().unwrap().last().unwrap();
    }

    result as usize
}

pub fn solution2(input: &str) -> usize {

    let mut result = 0;

    for line in input.lines() {

        let nums: Vec<i64> = line.split_ascii_whitespace().map(|v| v.parse().unwrap()).collect();
        let mut diffs: Vec<Vec<i64>> = vec![nums];

        // println!("{diffs:?}");

        while diffs.last().unwrap().iter().any(|v| v != &0) {
            let next: Vec<i64> = diffs.last().unwrap().windows(2).map(|v| v[1] - v[0]).collect();
            diffs.push(next);
        }

        diffs.reverse();

        for i in 0..diffs.len()-1 {
            let cur = *diffs[i].first().unwrap();
            let next = &mut diffs[i+1];
            let next_cur_val = *next.first().unwrap();
            let next_value = next_cur_val - cur;
            next.insert(0, next_value);
            // println!("{next_cur_val} - {cur} = {next_value}");
        }
        result += diffs.last().unwrap().first().unwrap();
    }

    result as usize
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day9/example.txt");

    #[test]
    fn test_s1() {
        let s = solution1(INPUT);
        assert_eq!(s, 114);
    }

    #[test]
    fn test_s2() {
        let s = solution2(INPUT);
        assert_eq!(s, 2);
    }
}
