use std::str::FromStr;
use anyhow::{
    Result,
    Error,
    anyhow,
};

#[derive(Debug)]
enum Color {
    Red,
    Green,
    Blue,
}

impl FromStr for Color {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "red" => Ok(Color::Red),
            "green" => Ok(Color::Green),
            "blue" => Ok(Color::Blue),
            no => Err(anyhow!("{no} is not a creative color")),
        } 
    }
}

#[derive(Debug)]
struct Cubes {
    count: usize,
    color: Color,
}

impl FromStr for Cubes {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (n_str, color_str) = s.split_once(" ").ok_or(anyhow!("invalid cubes {s}"))?;
        Ok(
            Cubes {
                count: n_str.parse()?,
                color: color_str.parse()?,
            }
        )
    }
}

#[derive(Debug)]
struct Game {
    id: usize,
    sets: Vec<Vec<Cubes>>,
}

impl FromStr for Game {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (game_id, rest) = s.split_once(":").ok_or(anyhow!("invalid {s}"))?;
        let (_, game_id) = game_id.split_once(" ").ok_or(anyhow!("invalid {game_id}"))?;
        
        let sets: Result<Vec<Vec<Cubes>>> = rest.split(";").map(|set_str| {
            let set: Result<Vec<Cubes>> = set_str.split(",").map(|s| s.trim()).map(|s| s.parse::<Cubes>()).collect();
            set
        }).collect();

        Ok(Game {
            id: game_id.parse()?,
            sets: sets?,
        })
    }
}

trait PossibleMatcher {
    fn matches<const R: usize, const G: usize, const B: usize>(&self) -> bool;
}

impl PossibleMatcher for Cubes {
    fn matches<const R: usize, const G: usize, const B: usize>(&self) -> bool {
        let count = match self.color {
            Color::Red => R,
            Color::Green => G,
            Color::Blue => B,
        };

        self.count <= count
    }
}

impl PossibleMatcher for Game {
    fn matches<const R: usize, const G: usize, const B: usize>(&self) -> bool {
        self.sets.iter().all(|s| s.iter().all(|c| c.matches::<R, G, B>()))
    }
}

#[derive(Debug, Default)]
struct CubeSet {
    red: usize,
    green: usize,
    blue: usize,
}

impl From<&Cubes> for CubeSet {
    fn from(value: &Cubes) -> Self {
        match value.color {
            Color::Red => CubeSet { red: value.count, ..Default::default() },
            Color::Green => CubeSet { green: value.count, ..Default::default() },
            Color::Blue => CubeSet { blue: value.count, ..Default::default() },
        }
    }
}

impl CubeSet {

    fn max(self, cube_set: &CubeSet) -> CubeSet {
        use std::cmp::max;

        CubeSet {
            red: max(self.red, cube_set.red),
            green: max(self.green, cube_set.green),
            blue: max(self.blue, cube_set.blue),
        }
    }

    fn max_cubes(self, cubes: &Cubes) -> CubeSet {
        self.max(&cubes.into())
    }

    fn score(&self) -> usize {
        self.red * self.green * self.blue
    }

}

trait FewFinder {
    fn fewest(&self) -> CubeSet;
}

impl FewFinder for Game {
    fn fewest(&self) -> CubeSet {
        self.sets.iter().fold(CubeSet::default(), |outer_acc, cubes_set| {
            let comp_cs = cubes_set.iter().fold(CubeSet::default(), |inner_acc, cubes| inner_acc.max_cubes(cubes));
            outer_acc.max(&comp_cs)
        })
    }
}

pub fn solution1<const R: usize, const G: usize, const B: usize>(input: &str) -> Result<usize> 
{
    let games: Result<Vec<Game>> = input.lines().map(|s| s.trim_end().parse()).collect();
    let games = games?;

    Ok(games.into_iter().filter(|g| g.matches::<R, G, B>()).map(|g| g.id).sum())
}

pub fn solution2(input: &str) -> Result<usize> {
    let games: Result<Vec<Game>> = input.lines().map(|s| s.trim_end().parse()).collect();
    let games = games?;

    Ok(games.into_iter().map(|g| g.fewest()).map(|cs| cs.score()).sum())
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day2/example.txt");

    #[test]
    fn test_read_example() {
        let games: Result<Vec<Game>> = INPUT.lines().map(|s| s.trim_end().parse()).collect();
        println!("{games:#?}");
    }

    #[test]
    fn test_solution1() {
        let solution = solution1::<12, 13, 14>(INPUT);
        println!("{solution:?}");
    }

    #[test]
    fn test_solution2() {
        let solution = solution2(INPUT);
        println!("{solution:?}");
    }

}

