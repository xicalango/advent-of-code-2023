
#[derive(Debug)]
pub enum SolutionResult {
    One(usize),
    Combined((usize, usize)),
}

impl From<usize> for SolutionResult {
    fn from(value: usize) -> Self {
        SolutionResult::One(value)
    }
}

impl From<(usize, usize)> for SolutionResult {
    fn from(value: (usize, usize)) -> Self {
        Self::Combined(value)
    }
}

pub trait SolutionReporter {
    fn preprocess<R, F: FnOnce() -> R>(&mut self, f: F) -> R;

    fn run<R: Into<SolutionResult>, F: FnOnce() -> R>(&mut self, f: F);

    fn run_one<R: Into<SolutionResult>, F: FnOnce(&str) -> R>(&mut self, data: &str, f: F) {
        self.run(|| f(data));
    }

    fn run_all<R: Into<SolutionResult>, F1: FnOnce(&str) -> R, F2: FnOnce(&str) -> R>(&mut self, data: &str, f1: F1, f2: F2) {
        self.run_one(data, f1);
        self.run_one(data, f2);
    }
}

pub trait SolutionId {
    fn id() -> usize;
}

pub trait Solution {
    fn main(r: &mut impl SolutionReporter);
}

pub struct Day<const N: usize>;

impl<const N: usize> SolutionId for Day<N> {
    fn id() -> usize {
        N
    }
}
