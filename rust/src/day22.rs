use std::{str::FromStr, ops::Index, collections::{HashMap, HashSet}};

use anyhow::{Error, anyhow};

use crate::utils::{coor3d::{Line3d, Component, StraightLine3d, Line3dAccess, Line3dAccessMut}, grid::{Grid2D, Grid2DAccess, Grid2DAccessMut}};


#[derive(Debug)]
struct Lines(Vec<StraightLine3d>);

impl FromStr for Lines {
    type Err = Error;

    fn from_str(s: &str) -> std::prelude::v1::Result<Self, Self::Err> {
        let res: Result<Vec<Line3d>, _> = s.lines().map(|l| l.parse()).collect();
        let mut lines = res?;
        lines.sort_by_key(|l| l.start.z.min(l.end.z));
        let lines: Result<Vec<StraightLine3d>, _> = lines.into_iter().map(|l| l.try_into()).collect();
        Ok(Lines(lines.map_err(|e| anyhow!("invalid: {e:?}"))?))
    }
}

impl Lines {

    fn lines(&self) -> &[StraightLine3d] {
        &self.0
    }

    fn lines_mut(&mut self) -> &mut [StraightLine3d] {
        &mut self.0
    }

    fn min_max(&self) -> Vec<[isize; 2]> {
        Component::all().iter().map(|comp| {
                    self.lines().iter().map(|line| {
                        let line_start = line.start();
                        let line_end = line.end();
                        let min = line_start.index(*comp).min(line_end.index(*comp));
                        let max = line_start.index(*comp).max(line_end.index(*comp));
                        [*min, *max]
                    })
                    .reduce(|[min1, max1], [min2, max2]| {
                        [min1.min(min2), max1.max(max2)]
                    })
                    .unwrap()
                }).collect()
    }

    fn fall(&mut self) {
        
        let min_max = self.min_max();
        let max_x = min_max[0][1] as usize;
        let max_y = min_max[1][1] as usize;

        let mut fall_index = Grid2D::new(max_x + 1, max_y + 1, 1usize);

        for line in self.lines_mut() {

            match line.component() {
                Component::X | Component::Y => {
                    let z_index = line.coors().map(|c| {
                        let c = (c.x as usize, c.y as usize);
                        fall_index.get_coor(&c)
                    }).max().flatten().unwrap().to_owned();

                    for coor in line.coors() {
                        let (ix, iy) = coor.drop_component(Component::Z);
                        fall_index.replace(ix as usize, iy as usize, z_index + 1);
                    }

                    line.move_line_with(|cur| cur.with(Component::Z, z_index as isize));
                },
                Component::Z => {
                    let start = line.start();
                    let (ix, iy, iz) = (start.x, start.y, start.z);

                    let z_index = fall_index.get(ix as usize, iy as usize).unwrap().to_owned();
                    #[cfg(any(test, feature = "debug"))]
                    println!("{z_index} -> {iz}");

                    let diff_z = z_index as isize - iz;

                    #[cfg(any(test, feature = "debug"))]
                    println!("{z_index} -> {iz} (delta {diff_z})");


                    line.move_line_with(|cur| cur.with_adjusted(Component::Z, |z| z + diff_z));
                    fall_index.replace(ix as usize, iy as usize, line.start()[Component::Z] as usize + 1);
                },
            }

        }
    }

    fn can_be_disintegrates(&self) -> usize {

        let mut occupancy: HashMap<(isize, isize, isize), usize> = HashMap::new();

        for (i, line) in self.lines().iter().enumerate() {
            for coor in line.coors() {
                occupancy.insert(coor.into(), i);
            }
        }

        let mut touchy: HashMap<usize, HashSet<usize>> = HashMap::new();

        for (i, line) in self.lines().iter().enumerate() {
            for coor in line.coors() {
                if let Some(&id) = occupancy.get(&coor.with_adjusted(Component::Z, |v| v-1).into()) {
                    if id != i {
                        touchy.entry(i).or_default().insert(id);
                    }
                }
            }
        }

        #[cfg(any(test, feature = "debug"))]
        println!("touchy: {touchy:?}");

        let mut removy: HashSet<_> = (0..self.lines().len()).collect();
        for v in touchy.values().filter(|v| v.len() == 1).flat_map(|v| v.iter()) {
            #[cfg(any(test))]
            println!("removy not {v}");
            removy.remove(v);
        }

        #[cfg(any(test, feature = "debug"))]
        {
            println!("only {removy:?}");
            println!("{}", removy.len());
        }

        removy.len()
    }


    fn draw_xz(&self) {
        let min_max = self.min_max();
        let max_x = min_max[0][1] as usize + 1;
        let max_z = min_max[2][1] as usize + 1;

        let mut occupied = Grid2D::new(max_x, max_z, '.');

        for (id, line) in self.lines().iter().enumerate() {
            for coor in line.coors() {
                occupied.replace(coor.x as usize, max_z - coor.z as usize, ((id%26)+65) as u8 as char);
            }
        }

        println!("{}", occupied.display());
    }

    fn draw_yz(&self) {
        let min_max = self.min_max();
        let max_y = min_max[1][1] as usize + 1;
        let max_z = min_max[2][1] as usize + 1;

        let mut occupied = Grid2D::new(max_y, max_z, '.');

        for (id, line) in self.lines().iter().enumerate() {
            for coor in line.coors() {
                occupied.replace(coor.y as usize, max_z - coor.z as usize, ((id%26)+65) as u8 as char);
            }
        }

        println!("{}", occupied.display());
    }

}

pub fn solution1(input: &str) -> usize {
    let mut lines: Lines = input.parse().unwrap();
    
    if cfg!(any(test, feature = "debug")) {
        println!("x");
        lines.draw_xz();
        println!("y");
        lines.draw_yz();
    }

    lines.fall();

    if cfg!(any(test, feature = "debug")) {
        println!("x");
        lines.draw_xz();
        println!("y");
        lines.draw_yz();
    }


    lines.can_be_disintegrates()
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day22/example.txt");

    #[test]
    fn test() {
        let mut lines: Lines = INPUT.parse().unwrap();

        println!("{lines:?}");

        println!("x");
        lines.draw_xz();
        println!("y");
        lines.draw_yz();

        lines.fall();

        println!("x");
        lines.draw_xz();
        println!("y");
        lines.draw_yz();

        let s = lines.can_be_disintegrates();
        assert_eq!(s, 5);
    }

}

