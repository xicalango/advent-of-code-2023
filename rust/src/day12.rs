
use std::{fmt::Display, iter::repeat, collections::HashMap};

use anyhow::{anyhow, Error, Result};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum SpringState {
    Broken,
    Operational,
    Unknown,
}

impl TryFrom<char> for SpringState {
    type Error = Error;

    fn try_from(value: char) -> Result<Self> {
        match value {
            '#' => Ok(SpringState::Broken),
            '.' => Ok(SpringState::Operational),
            '?' => Ok(SpringState::Unknown),
            e => Err(anyhow!("invalid spring state: {e}")),
        }
    }
}

impl SpringState {

    fn char(&self) -> char {
        match self {
            SpringState::Broken => '#',
            SpringState::Operational => '.',
            SpringState::Unknown => '?',
        }
    }

    fn is_operational(&self) -> bool {
        match self {
            SpringState::Operational | Self::Unknown => true,
            _ => false,
        }
    }

}

fn parse_states(states_str: &str) -> Result<Vec<SpringState>> {
    states_str.chars().map(|c| c.try_into()).collect()
}

fn parse_configs(configs_str: &str) -> Result<Vec<usize>> {
    configs_str.split(",")
               .map(|s| s.trim().parse().map_err(|e| anyhow!("cannot parse {s}: {e}")))
               .collect()
}

fn parse_line<const EXPANSE: usize>(line: &str) -> Result<(Vec<SpringState>, Vec<usize>)> {
    let (states, configs) = line.split_once(' ').ok_or(anyhow!("invalid line {line}"))?;

    let states = repeat(states).take(EXPANSE).collect::<Vec<&str>>().join("?");
    let configs = repeat(configs).take(EXPANSE).collect::<Vec<&str>>().join(",");

    let states = parse_states(&states)?;
    let configs = parse_configs(&configs)?;

    Ok((states, configs))
}

#[derive(Debug)]
struct ShowState<'a>(&'a [SpringState]);

impl Display for ShowState<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let &ShowState(states) = self;
        for s in states {
            write!(f, "{}", s.char())?;
        }
        Ok(())
    }
}

fn find_matching_position(states: &[SpringState], start: usize, size: usize) -> Option<usize> {
    let mut match_size = 0;

    if start > 0 {
        if states[start - 1] == SpringState::Broken {
            return None;
        }
    }

    for (i, state) in states.iter().enumerate().skip(start) {
        match state {
            SpringState::Broken  | SpringState::Unknown => {
                match_size += 1;
                if match_size == size {
                    match states.get(i+1) {
                        Some(SpringState::Operational) | Some(SpringState::Unknown) | None => {
                            return Some(i - (size - 1));
                        },
                        _ => {},
                    }
                }
            },
            _ => {match_size = 0},
        }
    }

    return None;
}

struct IterContext<'a> {
    states: &'a [SpringState],
    cfgs: &'a [usize],

    xpos: Vec<usize>,

    cache: HashMap<(Vec<usize>, usize), usize>,
}

impl<'a> IterContext<'a> {

    fn new(states: &'a [SpringState], cfgs: &'a [usize]) -> IterContext<'a> {
        let xpos: Vec<usize> = states.iter().enumerate().filter(|(_, &s)| s == SpringState::Broken).map(|(i, _)| i).collect();

        IterContext {
            states,
            cfgs,
            xpos,
            cache: HashMap::new(),
        }
    }

    fn validate(&self, bound: &usize, cur: &[(usize, usize)]) -> bool {
        self.xpos.iter()
            .filter(|i| i < &bound)
            .all(|i| {
                cur.iter().any(|&(s, len)| {
                    let range = s..(s+len);
                    let c = range.contains(&i);
                    c
                })
            })
    }

    fn count_solutions(&mut self) -> usize {
        self.find_stuff(self.cfgs, 0, &mut Vec::new())
    }

    fn find_stuff(&mut self, cfgs: &[usize], start: usize, cur: &mut Vec<(usize, usize)>) -> usize {
        
        let access_tuple = (cfgs.to_owned(), start);
        if let Some(v) = self.cache.get(&access_tuple) {
            return *v;
        }
            
        let res = if let Some((cfg_len, tail)) = cfgs.split_first() {
            let mut result = 0;
            for i in start..self.states.len() {
                if self.states[i] == SpringState::Operational {
                    continue;
                }

                let next_dot = self.states[i..].iter()
                        .enumerate()
                        .find(|(_, &s)| s == SpringState::Operational)
                        .map(|(pos, _)| pos + i)
                        .unwrap_or(self.states.len());

                if let Some(pos) = find_matching_position(&self.states[..next_dot], i, *cfg_len) {
                    let next = pos + cfg_len + 1;
                    let next = next.min(self.states.len());

                    cur.push((pos, *cfg_len));
                    if self.validate(&next, &cur) {
                        result += self.find_stuff(tail, next, cur);
                    }
                    cur.pop();
                    
                }
            }

            result
        } else {
            self.states[start..].iter().all(|v| v.is_operational()) as usize
        };

        self.cache.insert(access_tuple, res);
        
        res
    }

}

pub fn solution<const EXPANSE: usize>(input: &str) -> usize {
    input.lines()
            .map(|l| parse_line::<EXPANSE>(l).unwrap())
            .map(|(states, cfgs)| IterContext::new(&states, &cfgs).count_solutions())
            .sum()
}

pub fn solution1(input: &str) -> usize {
    solution::<1>(input)
}

pub fn solution2(input: &str) -> usize {
    solution::<5>(input)
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day12/example.txt");

    #[test]
    fn test_s1() {
        let s = solution1(INPUT);
        assert_eq!(s, 21);
    }

    #[test]
    fn test_s2() {
        let s = solution2(INPUT);
        assert_eq!(s, 525152);
    }

}
