
use std::collections::HashMap;

type Map = HashMap<String, (String, String)>;
type InstructionsMap = (Vec<char>, Map);

fn parse_input(input: &str) -> (Vec<char>, Map) {
    let mut lines = input.lines().map(|l| l.trim());
    let instructions: Vec<char> = lines.next().unwrap().chars().collect();

    let mut map: HashMap<String, (String, String)> = HashMap::new();

    for line in lines.skip(1) {
        let (src, dsts) = line.split_once(" = ").unwrap();
        let (ldst, rdst) = dsts.split_once(",").unwrap();
        let ldst = &ldst[1..];
        let rdst = &rdst[1..rdst.len()-1];
        map.insert(src.to_string(), (ldst.to_string(), rdst.to_string()));
    }

    (instructions, map)
}

fn chose_next<'a>(instruction: &char, options: &'a(String, String)) -> &'a str {
    match instruction {
        'L' => &options.0,
        'R' => &options.1,
        e => panic!("invalid: {e}"),
    }
}

pub fn solution1(input: &str) -> usize {
    let (instructions, map) = parse_input(input);

    let mut cur = "AAA";

    for (step, instruction) in instructions.iter().cycle().enumerate() {
        if cur == "ZZZ" {
            return step;
        }
        cur = chose_next(&instruction, &map[cur]);
    }
    
    panic!()
}


fn find_az_path(im: &InstructionsMap, start: &str) -> usize {
    let mut cur = start;
    let (instructions, map) = im;

    for (step, instruction) in instructions.iter().cycle().enumerate() {
        if cur.ends_with('Z') {
            return step;
        }
        cur = chose_next(&instruction, &map[cur]);
    }

    panic!()
}

pub fn solution2(input: &str) -> usize {
    let im = parse_input(input);

    im.1.keys()
        .filter(|v| v.ends_with('A'))
        .map(|v| find_az_path(&im, v))
        .reduce(|v1, v2| num_integer::lcm(v1, v2))
        .unwrap()
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT1: &'static str = include_str!("../../data/day8/example1.txt");
    const INPUT2: &'static str = include_str!("../../data/day8/example2.txt");
    const INPUT3: &'static str = include_str!("../../data/day8/example3.txt");

    #[test]
    fn test_s1_e1() {
        let s = solution1(INPUT1);
        println!("{s}");
        assert_eq!(s, 2);
    }

    #[test]
    fn test_s1_e2() {
        let s = solution1(INPUT2);
        println!("{s}");
        assert_eq!(s, 6);
    }

    #[test]
    fn test_s2() {
        let s = solution2(INPUT3);
        println!("{s}");
        assert_eq!(s, 6);
    }
}
