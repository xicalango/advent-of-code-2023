use crate::utils::{coor3d::{Coor3d, Component}, linear::Lin2};

fn parse_hails(input: &str) -> Vec<(Coor3d, Coor3d, Lin2)> {
    let mut hails: Vec<(Coor3d, Coor3d, Lin2)> = Vec::new();

    for line in input.lines() {
        let (pos_str, vel_str) = line.split_once(" @ ").unwrap();
        let pos_vec: Vec<isize> = pos_str.split(",").map(|p| p.trim().parse().unwrap()).collect();
        let vel_vec: Vec<isize> = vel_str.split(",").map(|p| p.trim().parse().unwrap()).collect();

        let pos: Coor3d = pos_vec.try_into().unwrap();
        let vel: Coor3d = vel_vec.try_into().unwrap();

        let pos_2d = pos.drop_component(Component::Z);
        let vel_2d = vel.drop_component(Component::Z);

        let lin2 = Lin2::from_pv(&pos_2d, &vel_2d);

        hails.push((pos, vel, lin2));
    }

    hails
}


pub fn solution1<const MIN: usize, const MAX: usize>(input: &str) -> usize {

    let hails = parse_hails(input);

    let mut count = 0;

    for i in 0..hails.len() {
        let (p1, v1, h1) = &hails[i];

        for j in (i+1)..hails.len() {
            let (p2, v2, h2) = &hails[j];

            let intersection = h1.intersection(h2);

            if cfg!(any(test, feature = "debug")) {
                println!("{p1}, {v1} -> {h1:?}");
                println!("{p2}, {v2} -> {h2:?}");
                println!("{intersection:?}");
            }

            if let Some((x, y)) = intersection {
                if x >= MIN as f64 && x <= MAX as f64 && y >= MIN as f64 && y <= MAX as  f64 {
                    if (x - p1[Component::X] as f64).signum() as isize == v1[Component::X].signum() &&
                       (y - p1[Component::Y] as f64).signum() as isize == v1[Component::Y].signum() &&
                       (x - p2[Component::X] as f64).signum() as isize == v2[Component::X].signum() && 
                       (y - p2[Component::Y] as f64).signum() as isize == v2[Component::Y].signum() {
                            #[cfg(any(test, feature = "debug"))]
                            println!("{x}, {y}");
                            count += 1
                        }
                }
            }
            
        }
    }

    count
}

pub fn solution2(input: &str) {
    let hails = parse_hails(input);

    for hail in &hails {
        println!("{}", hail.2);
    }
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day24/example.txt");

    #[test]
    fn test_s1() {
        let s = solution1::<7, 27>(INPUT);
        assert_eq!(s, 2);
    }

    #[test]
    fn test_s2() {
        let _s = solution2(INPUT);
    }

}

