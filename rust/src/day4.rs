use std::collections::HashSet;

fn calc_wins(line: &str) -> usize {
    let (_, data) = line.split_once(":").unwrap();
    let (winning, mine) = data.split_once("|").unwrap();

    let winning: HashSet<&str> = winning.split_ascii_whitespace()
            .map(|v| v.trim()).collect();
    let mine: HashSet<&str> = mine.split_ascii_whitespace()
            .map(|v| v.trim()).collect();

    winning.intersection(&mine).count()
}

pub fn solution1(input: &str) -> usize {
    input.lines()
        .map(calc_wins)
        .filter(|&v| v > 0)
        .map(|v| 1 << v-1)
        .sum()
}

pub fn solution2(input: &str) -> usize {
    let mut copies = vec![1];
    for (i, line) in input.lines().enumerate() {
        if copies.len() < i + 1 {
            copies.resize(i + 1, 1);
        }

        let wins = calc_wins(line);

        if wins == 0 {
            continue;
        }

        if copies.len() < i + wins + 1 {
            copies.resize(i + wins + 1, 1);
        }
        for j in 1..wins + 1 {
            copies[i+j] += copies[i];
        }
    }

    copies.into_iter().sum()
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day4/example.txt");

    #[test]
    fn test_solution1() {
        let s = solution1(INPUT);
        println!("{s}");
        assert_eq!(s, 13);
    }

    #[test]
    fn test_solution2() {
        let s = solution2(INPUT);
        println!("{s}");
        assert_eq!(s, 30);
    }
}
