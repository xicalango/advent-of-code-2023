use std::{ops::{RangeInclusive, Sub, Range}, fmt::Debug};
use crate::utils::num::{Decrement, Increment};

pub enum SplitOff<T> {
    NoOverlap(T),
    SplitContainsFully(T),
    TwoSplit(T, T),
    ThreeSplit(T, T, T),
}

impl<T: Debug> Debug for SplitOff<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::NoOverlap(arg0) => f.debug_tuple("NoOverlap").field(arg0).finish(),
            Self::SplitContainsFully(arg0) => f.debug_tuple("SplitContainsFully").field(arg0).finish(),
            Self::TwoSplit(arg0, arg1) => f.debug_tuple("TwoSplit").field(arg0).field(arg1).finish(),
            Self::ThreeSplit(arg0, arg1, arg2) => f.debug_tuple("ThreeSplit").field(arg0).field(arg1).field(arg2).finish(),
        }
    }
}

pub trait RangeExt {

    fn intersects(&self, other: &Self) -> bool where Self: Sized {
        self.intersect(other).is_some()
    }

    fn contains_fully(&self, other: &Self) -> bool;

    fn intersect(&self, other: &Self) -> Option<Self> where Self: Sized;

    fn subtract(&self, other: &Self) -> Option<Self> where Self: Sized;

    fn join(&self, other: &Self) -> (Self, Option<Self>) where Self: Sized;

    fn join_mut(&mut self, other: Self) -> Option<Self> where Self: Sized {
        let (joined, reminder) = self.join(&other);
        *self = joined;
        reminder
    }

    fn split_off(&self, other: &Self) -> SplitOff<Self> where Self: Sized;
}

impl<T> RangeExt for RangeInclusive<T>
where T: Ord + Copy + Decrement<Output = T> + Increment<Output = T>
{
    fn contains_fully(&self, other: &Self) -> bool {
        let intersection = self.intersect(other);
        if intersection.is_none() {
            return false;
        }
        let intersection = intersection.unwrap();

        return &intersection == self || &intersection == other;
    }

    fn intersect(&self, other: &Self) -> Option<Self> {
        let max_start = std::cmp::max(self.start(), other.start());
        let min_end = std::cmp::min(self.end(), other.end());

        if min_end >= max_start {
            Some(*max_start..=*min_end)
        } else {
            None
        }
    }

    fn subtract(&self, rhs: &Self) -> Option<Self> where Self: Sized {
        match self.intersect(rhs) {
            None => Some(self.clone()),
            Some(intersection) => {
                if self == &intersection {
                    None
                } else if self.start() == intersection.start() {
                    Some(*intersection.end()..=*self.end())
                } else {
                    Some(*self.start()..=*intersection.start())
                }
            }
        }
    }

    // 123 456 789
    //     456
    // 123 456 789

    // 123 456
    //     456 789
    // 123 456 789

    // 123
    //     456
    // 123 Some(456)

    // 123
    //         789
    // 123     Some(789)

    fn join(&self, other: &Self) -> (Self, Option<Self>) {
        if self.intersects(other) {
            let min_start = std::cmp::min(self.start(), other.start());
            let max_end = std::cmp::max(self.end(), other.end());
            return (*min_start..=*max_end, None);
        } else {
            (self.clone(), Some(other.clone()))
        }
    }

    fn split_off(&self, other: &Self) -> SplitOff<Self> where Self: Sized {
        let intersection = self.intersect(other);

        if intersection.is_none() {
            return SplitOff::NoOverlap(self.clone());
        }

        let intersection = intersection.unwrap();

        let mut split_start = None;
        let mut split_end = None;


        if intersection.start() != self.start() {
            split_start = Some(*self.start()..=intersection.start().dec());
        }

        if intersection.end() != self.end() {
            split_end = Some(intersection.end().inc()..=*self.end());
        }

        let split_intersection = intersection;

        match (split_start, split_end) {
            (None, None) => SplitOff::SplitContainsFully(split_intersection),
            (Some(s), None) => SplitOff::TwoSplit(s, split_intersection),
            (None, Some(e)) => SplitOff::TwoSplit(split_intersection, e),
            (Some(s), Some(e)) => SplitOff::ThreeSplit(s, split_intersection, e),
        }

    }
}

pub trait RangeLength {
    type Output;
    fn len(&self) -> Self::Output;
}

impl<T> RangeLength for RangeInclusive<T>
where T: Sub<T, Output=T> + Increment<Output=T> + Copy {
    type Output = T;

    fn len(&self) -> Self::Output {
        self.end().inc() - *self.start()
    }
}

impl<T> RangeLength for Range<T>
where T: Sub<T, Output=T> + Copy {
    type Output = T;

    fn len(&self) -> Self::Output {
        self.end - self.start
    }
}
