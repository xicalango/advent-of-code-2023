use std::{fmt::Display, str::FromStr, ops::{Index, IndexMut, Add, Sub, AddAssign}, borrow::{Cow, Borrow}};

use anyhow::{anyhow, Error, Result};

use super::coor::ICoor;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
pub enum Component {
    X,
    Y,
    Z,
}

impl Display for Component {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let c = match self {
            Component::X => 'X',
            Component::Y => 'Y',
            Component::Z => 'Z',
        };
        write!(f, "{c}")
    }
}

impl TryFrom<char> for Component {
    type Error = Error;

    fn try_from(value: char) -> std::prelude::v1::Result<Self, Self::Error> {
        let comp = match value {
            'X' => Component::X,
            'Y' => Component::Y,
            'Z' => Component::Z,
            e => return Err(anyhow!("invalid component: {e}")),
        };
        Ok(comp)
    }
}

impl Component {

    pub fn all() -> [Component; 3] {
        [Component::X, Component::Y, Component::Z]
    }

    pub fn next(&self) -> Option<Component> {
        match self {
            Component::X => Some(Component::Y),
            Component::Y => Some(Component::Z),
            Component::Z => None,
        }
    }

}

impl From<super::coor::Component> for Component {
    fn from(value: super::coor::Component) -> Self {
        match value {
            super::coor::Component::X => Component::X,
            super::coor::Component::Y => Component::Y,
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub struct Coor3d {
    pub x: isize,
    pub y: isize, 
    pub z: isize,
}

impl Display for Coor3d {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{},{},{}", self.x, self.y, self.z)
    }
}

impl From<(isize, isize, isize)> for Coor3d {
    fn from(value: (isize, isize, isize)) -> Self {
        let (x, y, z) = value;
        Coor3d { x, y, z }
    }
}

impl From<[isize; 3]> for Coor3d {
    fn from(value: [isize; 3]) -> Self {
        Coor3d { x: value[0], y: value[1], z: value[2] }
    }
}

impl From<Coor3d> for (isize, isize, isize) {
    fn from(value: Coor3d) -> Self {
        (value.x, value.y, value.z)
    }
}

impl From<Coor3d> for [isize; 3] {
    fn from(value: Coor3d) -> Self {
        [value.x, value.y, value.z]
    }
}

impl TryFrom<Vec<isize>> for Coor3d {
    type Error = Vec<isize>;

    fn try_from(value: Vec<isize>) -> Result<Self, Self::Error> {
        let r: Result<[isize; 3], _> = value.try_into();
        r.map(|v| v.into())
    }
}

impl FromStr for Coor3d {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let vec: Result<Vec<isize>, _> = s.split(",").map(|v| v.parse()).collect();
        Ok(vec?.try_into().map_err(|e| anyhow!("cannot convert {e:?}"))?)
    }
}

impl Index<Component> for Coor3d {
    type Output = isize;

    fn index(&self, index: Component) -> &Self::Output {
        match index {
            Component::X => &self.x,
            Component::Y => &self.y,
            Component::Z => &self.z,
        }
    }
}

impl IndexMut<Component> for Coor3d {
    fn index_mut(&mut self, index: Component) -> &mut Self::Output {
        match index {
            Component::X => &mut self.x,
            Component::Y => &mut self.y,
            Component::Z => &mut self.z,
        }
    }
}

impl Add<Coor3d> for Coor3d {
    type Output = Coor3d;

    fn add(self, rhs: Coor3d) -> Self::Output {
        Coor3d {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl AddAssign<Coor3d> for Coor3d {
    fn add_assign(&mut self, rhs: Coor3d) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl Sub<Coor3d> for Coor3d {
    type Output = Coor3d;

    fn sub(self, rhs: Coor3d) -> Self::Output {
        Coor3d {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl Coor3d {

    pub fn new(x: isize, y: isize, z: isize) -> Coor3d {
        Coor3d { x, y, z }
    }

    pub fn components(&self) -> Components<'_> {
        Components { coor: self, comp: Some(Component::X) }
    }

    pub fn with(&self, c: Component, v: isize) -> Coor3d {
        self.with_adjusted(c, |_| v)
    }

    pub fn with_adjusted<F: FnOnce(isize) -> isize>(&self, c: Component, f: F) -> Coor3d {
        match c {
            Component::X => Coor3d { x: f(self.x), ..*self },
            Component::Y => Coor3d { y: f(self.y), ..*self },
            Component::Z => Coor3d { z: f(self.z), ..*self },
        }
    }

    pub fn drop_component(&self, c: Component) -> ICoor {
        match c {
            Component::X => (self.y, self.z),
            Component::Y => (self.x, self.z),
            Component::Z => (self.x, self.y),
        }
    }

}

pub struct Components<'a> {
    coor: &'a Coor3d,
    comp: Option<Component>,
}

impl<'a> Iterator for Components<'a> {
    type Item = (Component, isize);

    fn next(&mut self) -> Option<Self::Item> {
        let res = match self.comp {
            Some(c) => {
                let comp = self.coor[c];
                Some((c, comp))
            },
            None => None,
        };

        self.comp = res.clone().and_then(|(c, _)| c.next());
        res
    }
}

pub trait Line3dAccess {
    fn start(&self) -> Cow<Coor3d>;
    fn end(&self) -> Cow<Coor3d>;

    fn changes(&self) -> Vec<(Component, isize)> {
        self.start().components().zip(self.end().components())
            .map(|((c, c1), (_, c2))| (c, c2 - c1))
            .filter(|(_, c)| c != &0)
            .collect()
    }

    fn straight_line_len(&self) -> Option<(Component, isize)> {
        match &self.changes()[..] {
            &[] => Some((Component::X, 0)),
            &[c] => Some(c),
            _ => None,
        }
    }
}

pub trait Line3dAccessMut : Line3dAccess {

    fn move_line_to(&mut self, v: Coor3d);

    fn move_line(&mut self, v: Coor3d) {
        self.move_line_to((*self.start()).clone() + v);
    }

    fn move_line_with<F: FnOnce(&Coor3d) -> Coor3d>(&mut self, f: F) {
        self.move_line_to(f(self.start().borrow()))
    }

}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Line3d {
    pub start: Coor3d,
    pub end: Coor3d,
}

impl FromStr for Line3d {
    type Err = Error;

    fn from_str(s: &str) -> std::prelude::v1::Result<Self, Self::Err> {
        let (start, end) = s.split_once('~').ok_or(anyhow!("invalid: {s}"))?;
        Ok(Line3d {
            start: start.parse()?,
            end: end.parse()?,
        })
    }
}

impl Line3d {

    pub fn from_line(line: impl Line3dAccess) -> Line3d {
        Line3d { start: (*line.start()).to_owned(), end: (*line.end()).to_owned() }
    }

}

impl Line3dAccess for Line3d {
    fn start(&self) -> Cow<Coor3d> {
        Cow::Borrowed(&self.start)
    }

    fn end(&self) -> Cow<Coor3d> {
        Cow::Borrowed(&self.end)
    }
}

impl Line3dAccessMut for Line3d {
    fn move_line_to(&mut self, v: Coor3d) {
        let diff = v.clone() - self.start.clone();
        self.start = v;
        self.end += diff;
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct StraightLine3d {
    start: Coor3d,
    component: Component,
    len: usize,    
}

impl TryFrom<Line3d> for StraightLine3d {
    type Error = Line3d;

    fn try_from(value: Line3d) -> std::prelude::v1::Result<Self, Self::Error> {
        if let Some((component, len)) = value.straight_line_len() {
            if len < 0 {
                let start = value.start.with_adjusted(component, |v| v-len);
                Ok(StraightLine3d { start: start, component, len: len as usize })
            } else {
                Ok(StraightLine3d { start: value.start, component, len: len as usize })                
            }
        } else {
            Err(value)
        }
    }
}

impl From<StraightLine3d> for Line3d {
    fn from(value: StraightLine3d) -> Self {
        Line3d::from_line(value)
    }
}

impl Line3dAccess for StraightLine3d {
    fn start(&self) -> Cow<Coor3d> {
        Cow::Borrowed(&self.start)
    }

    fn end(&self) -> Cow<Coor3d> {
        Cow::Owned(self.start.with_adjusted(self.component, |v| v.checked_add_unsigned(self.len).unwrap()))
    }
}

impl Line3dAccessMut for StraightLine3d {
    fn move_line_to(&mut self, v: Coor3d) {
        self.start = v;
    }
}

impl StraightLine3d {

    pub fn coors(&self) -> Coors {
        Coors { line: self, cur: 0 }
    }

    pub fn len(&self) -> &usize {
        &self.len
    }

    pub fn component(&self) -> &Component {
        &self.component
    }

}


pub struct Coors<'a> {
    line: &'a StraightLine3d,
    cur: usize, 
}

impl<'a> Iterator for Coors<'a> {
    type Item = Coor3d;

    fn next(&mut self) -> Option<Self::Item> {
        if self.cur > self.line.len {
            None
        } else {
            let coor = self.line.start.with_adjusted(self.line.component, |v| v.checked_add_unsigned(self.cur).unwrap());
            self.cur += 1;
            Some(coor)
        }
    }
}

#[cfg(test)]
mod test{
    use super::*;


    #[test]
    fn test_straight_lines() {
        let line: Line3d = "1,1,8~1,1,9".parse().unwrap();
        let straight_line: Result<StraightLine3d, _> = line.try_into();
        assert_eq!(Ok(StraightLine3d{ start: [1,1,8].into(), component: Component::Z, len: 1 }), straight_line);

        let line: Line3d = "1,1,8~1,1,8".parse().unwrap();
        let straight_line: Result<StraightLine3d, _> = line.try_into();
        assert_eq!(Ok(StraightLine3d{ start: [1,1,8].into(), component: Component::X, len: 0 }), straight_line);

        let line: Line3d = "1,1,8~1,2,9".parse().unwrap();
        let straight_line: Result<StraightLine3d, _> = line.try_into();
        assert!(straight_line.is_err());
    }

}
