
use std::{usize, fmt::Display, ops::{Index, IndexMut}};

use anyhow::{anyhow, Error};

use super::direction::{Walkable, Direction, TryWalkable};

pub type Coor<T> = (T, T);

pub type UCoor = Coor<usize>;
pub type ICoor = Coor<isize>;
pub type F64Coor = Coor<f64>;

impl Walkable for UCoor {

    fn walk(&self, dir: &Direction) -> UCoor {
        use Direction::*;
        let (x, y) = self;
        match dir {
            North => (*x, y.saturating_add_signed(-1)),
            East => (x+1, *y),
            South => (*x, y+1),
            West => (x.saturating_add_signed(-1), *y),
        }
    }

}

impl TryWalkable for UCoor {
    fn try_walk(&self, dir: &Direction) -> Option<Self> where Self: Sized {
        let (x, y) = self;

        match (x, y, dir) {
            (0, _, Direction::West) |
            (_, 0, Direction::North) |
            (&usize::MAX, _, Direction::East) |
            (_, &usize::MAX, Direction::South) => None,
            _ => Some(self.walk(dir))
        }
    }
}


impl Walkable for ICoor {

    fn walk(&self, dir: &Direction) -> ICoor {
        use Direction::*;
        let (x, y) = self;
        match dir {
            North => (*x, y-1),
            East => (x+1, *y),
            South => (*x, y+1),
            West => (x-1, *y),
        }
    }

}

impl TryWalkable for ICoor {
    fn try_walk(&self, dir: &Direction) -> Option<Self> where Self: Sized {
        Some(self.walk(dir))
    }
}


#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
pub enum Component {
    X,
    Y,
}

impl Display for Component {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let c = match self {
            Component::X => 'X',
            Component::Y => 'Y',
        };
        write!(f, "{c}")
    }
}

impl TryFrom<char> for Component {
    type Error = Error;

    fn try_from(value: char) -> std::prelude::v1::Result<Self, Self::Error> {
        let comp = match value {
            'X' => Component::X,
            'Y' => Component::Y,
            e => return Err(anyhow!("invalid component: {e}")),
        };
        Ok(comp)
    }
}

impl Component {

    pub fn all() -> [Component; 2] {
        [Component::X, Component::Y]
    }

    pub fn next(&self) -> Option<Component> {
        match self {
            Component::X => Some(Component::Y),
            Component::Y => None,
        }
    }

}

impl TryFrom<super::coor3d::Component> for Component {
    type Error = super::coor3d::Component;

    fn try_from(value: super::coor3d::Component) -> Result<Self, Self::Error> {
        match value {
            super::coor3d::Component::X => Ok(Component::X),
            super::coor3d::Component::Y => Ok(Component::Y),
            super::coor3d::Component::Z => Err(value),
        }
    }
}

impl<T> Index<Component> for Coor<T> {
    type Output = T;

    fn index(&self, index: Component) -> &Self::Output {
        match index {
            Component::X => &self.0,
            Component::Y => &self.1,
        }
    }
}

impl<T> IndexMut<Component> for Coor<T> {
    fn index_mut(&mut self, index: Component) -> &mut Self::Output {
        match index {
            Component::X => &mut self.0,
            Component::Y => &mut self.1,
        }
    }
}
