use super::coor::ICoor;


#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {

    pub fn connects_with(&self, other: &Direction) -> bool {
        use Direction::*;
        match (self, other) {
           (North, South) | (South, North) => true, 
           (East, West) | (West, East) => true, 
           _ => false,
        }
    }

    pub fn invert(&self) -> Direction {
        match self {
            Direction::North => Direction::South,
            Direction::East => Direction::West,
            Direction::South => Direction::North,
            Direction::West => Direction::East,
        }
    }

    pub fn all() -> [Direction; 4] {
        use Direction::*;
        [North, East, South, West]
    }

    pub fn walk<W: Walkable>(&self, walkable: &W) -> W {
        walkable.walk(self)
    }

    pub fn try_walk<W: TryWalkable>(&self, walkable: &W) -> Option<W> {
        walkable.try_walk(self)
    }
}

pub trait Walkable : TryWalkable {
    fn walk(&self, dir: &Direction) -> Self;
}

pub trait TryWalkable {
    fn try_walk(&self, dir: &Direction) -> Option<Self> where Self: Sized;
}

impl From<Direction> for ICoor {
    fn from(value: Direction) -> Self {
        match value {
            Direction::North => (0, -1),
            Direction::East => (1, 0),
            Direction::South => (0, 1),
            Direction::West => (-1, 0),
        }
    }
}
