use std::fmt::Display;

use super::coor::{ICoor, F64Coor};

#[derive(Debug)]
pub struct Lin2 {
    a: f64,
    b: f64,
}

impl Lin2 {

    pub fn new(a: f64, b: f64) -> Lin2 {
        Lin2 { a, b }
    }

    pub fn from_pv(pos: &ICoor, vel: &ICoor) -> Lin2 {
        let (x0, y0) = *pos;
        let (vx, vy) = *vel;
        let a: f64 = (vy as f64) / (vx as  f64);
        let b = (y0 as f64) - a * (x0 as f64);
        Lin2 { a, b }
    }

    pub fn intersection(&self, other: &Lin2) -> Option<F64Coor> {
        if self.a == other.a {
            return None;
        }

        let x = (other.b - self.b) / (self.a - other.a);
        let y = self.f(x);

        Some((x, y))
    }

    pub fn f(&self, x: f64) -> f64 {
        self.a * x + self.b
    }

    pub fn f_inv(&self, y: f64) -> f64 {
        (y - self.b) / self.a
    }

}

impl Display for Lin2 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "f(x) = {:?} * x + {:?}", self.a, self.b)
    }
}