use std::{collections::{HashMap, BinaryHeap}, hash::Hash, ops::Add};

pub trait Graph {
    type VertexId;
    type Cost;

    fn neighbors(&self, vertex: &Self::VertexId) -> Vec<(Self::VertexId, Self::Cost)>;
}

struct DijkstraState<G: Graph> {
    vertex: G::VertexId,
    cost: G::Cost,
}

impl<G: Graph> Ord for DijkstraState<G> 
where G::Cost: Ord, G::VertexId: Ord {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.cost.cmp(&self.cost).then_with(|| self.vertex.cmp(&other.vertex))
    }
}

impl<'a, G: Graph> PartialOrd for DijkstraState<G> 
where G::Cost: Ord, G::VertexId: Ord {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a, G: Graph> Eq for DijkstraState<G> 
where G::Cost: Eq, G::VertexId: Eq {
}

impl<'a, G: Graph> PartialEq for DijkstraState<G> 
where 
    G::Cost: PartialEq,
    G::VertexId: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.vertex == other.vertex && self.cost == other.cost
    }
}


pub struct Dijkstra<'a, G: Graph> {
    graph: &'a G,
    dist: HashMap<G::VertexId, (G::Cost, Option<G::VertexId>)>,
    heap: BinaryHeap<DijkstraState<G>>,
}

impl<'a, G: Graph> Dijkstra<'a, G> 
where
    G::Cost: Ord,
    G::VertexId: Ord,
{

    pub fn new(graph: &'a G) -> Dijkstra<G> {
        Dijkstra { graph, dist: Default::default(), heap: Default::default() }
    }

}

impl<'a, G: Graph> Dijkstra<'a, G> 
where
    G::Cost: Ord + Default + Add<G::Cost, Output = G::Cost> + Clone,
    G::VertexId: Ord + Hash + Clone,
{

    pub fn sssp(mut self, start: &'a G::VertexId, dest: &G::VertexId) -> Option<HashMap<G::VertexId, (G::Cost, Option<G::VertexId>)>> {

        self.dist.insert(start.clone(), (Default::default(), None));
        self.heap.push(DijkstraState { vertex: start.clone(), cost: Default::default() });

        while let Some(DijkstraState { vertex, cost }) = self.heap.pop() {
            if &vertex == dest {
                return Some(self.dist);
            }

            if cost > self.dist[&vertex].0 {
                continue;
            }

            for (next_vertex, next_cost) in self.graph.neighbors(&vertex) {
                if self.dist.contains_key(&next_vertex) {
                    continue;
                }

                let next: DijkstraState<G> = DijkstraState { vertex: next_vertex, cost: cost.clone() + next_cost };

                if next.cost < self.dist[&vertex].0 {
                    self.dist.insert(next.vertex.clone(), (next.cost.clone(), Some(vertex.clone())));
                    self.heap.push(next);

                }
            }
        }

        None
    }

}
