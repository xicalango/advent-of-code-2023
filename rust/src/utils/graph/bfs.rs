use std::{collections::{VecDeque, HashSet, HashMap}, hash::Hash, rc::Rc, fmt::Debug};

#[derive(Debug)]
pub enum CallbackResult {
    Continue,
    Break,
}

#[derive(Debug)]
pub enum BfsResult {
    CallbackTriggered,
    VisitedAll,
}

pub trait Graph {
    type VertexId;

    fn neighbors(&self, vertex: &Self::VertexId) -> Vec<Self::VertexId>;
}

pub struct Bfs<'a, G: Graph> {
    graph: &'a mut G,
    frontier: VecDeque<(G::VertexId, usize, Option<Rc<G::VertexId>>)>,
    visited: HashSet<G::VertexId>,
}

impl<'a, G: Graph> Bfs<'a, G> {
    pub fn new(graph: &'a mut G) -> Bfs<G> {
        Bfs { graph, frontier: VecDeque::new(), visited: HashSet::new() }
    }
}

impl<'a, G: Graph> Bfs<'a, G>
where G::VertexId: Eq + Hash + Clone {

    pub fn sssp(self, start: &'a G::VertexId, end: &G::VertexId) -> Option<HashMap<G::VertexId, (G::VertexId, usize)>> {

        let mut prev = HashMap::new();

        let result = self.bfs(start, |_, cur, level, prev_vertex|  {
            if let Some(prev_vertex) = prev_vertex {
                prev.insert(cur.clone(), ((*prev_vertex).clone(), level));
            }
            
            if cur == end {
                CallbackResult::Break
            } else {
                CallbackResult::Continue
            }
        });

        match result {
            BfsResult::CallbackTriggered => Some(prev),
            BfsResult::VisitedAll => None,
        }
    }

    pub fn all_dists(self, start: &G::VertexId) -> HashMap<G::VertexId, (G::VertexId, usize)> {
        self.all_dists_filtered(start, |_, _| true)
    }

    pub fn all_dists_filtered<F>(self, start: &G::VertexId, filter: F) -> HashMap<G::VertexId, (G::VertexId, usize)> 
    where F: Fn(&G, &G::VertexId) -> bool {

        let mut prev = HashMap::new();

        self.bfs_filtered(start, filter, |_, cur, level, prev_vertex| {
            if let Some(prev_vertex) = prev_vertex {
                prev.insert(cur.clone(), ((*prev_vertex).clone(), level));
            }
            CallbackResult::Continue
        });

        prev
    }

    pub fn bfs<F>(self, start: &G::VertexId, callback: F) -> BfsResult
    where F: FnMut(&mut G, &G::VertexId, usize, Option<Rc<G::VertexId>>) -> CallbackResult {
        self.bfs_filtered(start, |_, _| true, callback)
    }

    pub fn bfs_filtered<FCb, FFilter>(mut self, start: &G::VertexId, filter: FFilter, mut callback: FCb) -> BfsResult
    where FCb: FnMut(&mut G, &G::VertexId, usize, Option<Rc<G::VertexId>>) -> CallbackResult,
        FFilter: Fn(&G, &G::VertexId) -> bool {

        self.frontier.push_back((start.clone(), 0, None));
        self.visited.insert(start.clone());

        while let Some((cur, level, prev)) = self.frontier.pop_front() {
            match callback(&mut self.graph, &cur, level, prev) {
                CallbackResult::Break => {
                    return BfsResult::CallbackTriggered;
                }
                _ => {}
            }

            let cur_rc = Rc::new(cur);

            for neighbor in self.graph.neighbors(&cur_rc) {
                if !self.visited.contains(&neighbor) && filter(&self.graph, &neighbor) {
                    self.frontier.push_back((neighbor.clone(), level+1, Some(cur_rc.clone())));
                    self.visited.insert(neighbor.clone());
                }
            }
        }

        BfsResult::VisitedAll
    }

    pub fn iter_from(self, start: &'a G::VertexId) -> BfsIter<G, impl Fn(&G, &G::VertexId) -> bool> {
        self.iter_from_filtered(start, |_, _| true)
    }

    pub fn iter_from_filtered<F>(mut self, start: &'a G::VertexId, filter: F) -> BfsIter<G, F> 
    where F: Fn(&G, &G::VertexId) -> bool {
        self.frontier.push_back((start.clone(), 0, None));
        self.visited.insert(start.clone());
        BfsIter(self, filter)
    }

}

impl<'a, G: Graph + Debug> Debug for Bfs<'a, G> 
where G::VertexId: Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Bfs").field("graph", &self.graph).field("frontier", &self.frontier).field("visited", &self.visited).finish()
    }
}

pub struct BfsIter<'a, G: Graph, FFilter: Fn(&G, &G::VertexId) -> bool>(Bfs<'a, G>, FFilter);

impl<'a, G: Graph, FFilter: Fn(&G, &G::VertexId) -> bool> Iterator for BfsIter<'a, G, FFilter>
where G::VertexId: Eq + Hash + Clone {
    type Item = (Rc<G::VertexId>, usize, Option<Rc<G::VertexId>>);

    fn next(&mut self) -> Option<Self::Item> {
        let BfsIter(bfs, filter) = self;

        if bfs.frontier.is_empty() {
            return None;
        }

        let (cur, level, prev) = bfs.frontier.pop_front().unwrap();

        let cur_rc = Rc::new(cur);

        for neighbor in bfs.graph.neighbors(&cur_rc) {
            if !bfs.visited.contains(&neighbor) && filter(&bfs.graph, &neighbor) {
                bfs.frontier.push_back((neighbor.clone(), level+1, Some(cur_rc.clone())));
                bfs.visited.insert(neighbor.clone());
            }
        }

        Some((cur_rc, level, prev))
    }
}
