
pub mod ranges;
pub mod num;
pub mod grid;
pub mod direction;
pub mod coor;
pub mod coor3d;
pub mod linear;
pub mod graph;

#[allow(unused)]
pub fn get_n_threads() -> usize {
    std::env::var("N_THREADS").ok().and_then(|v| v.parse().ok()).unwrap_or(4)
}

pub const fn day_enabled(day: usize) -> bool {
    match day {
        1 => cfg!(feature = "day1"),
        2 => cfg!(feature = "day2"),
        3 => cfg!(feature = "day3"),
        4 => cfg!(feature = "day4"),
        5 => cfg!(feature = "day5"),
        6 => cfg!(feature = "day6"),
        7 => cfg!(feature = "day7"),
        8 => cfg!(feature = "day8"),
        9 => cfg!(feature = "day9"),
        10 => cfg!(feature = "day10"),
        11 => cfg!(feature = "day11"),
        12 => cfg!(feature = "day12"),
        13 => cfg!(feature = "day13"),
        14 => cfg!(feature = "day14"),
        15 => cfg!(feature = "day15"),
        16 => cfg!(feature = "day16"),
        17 => cfg!(feature = "day17"),
        18 => cfg!(feature = "day18"),
        19 => cfg!(feature = "day19"),
        20 => cfg!(feature = "day20"),
        21 => cfg!(feature = "day21"),
        22 => cfg!(feature = "day22"),
        23 => cfg!(feature = "day23"),
        24 => cfg!(feature = "day24"),
        25 => cfg!(feature = "day25"),
        _ => panic!(),
    }
}
