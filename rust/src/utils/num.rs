use std::ops::{Add, Sub};

pub trait Decrement {
    type Output;

    fn dec(self) -> Self::Output;
}

pub trait Increment {
    type Output;

    fn inc(self) -> Self::Output;
}

pub trait Zero {
    fn zero() -> Self;
}

pub trait One {
    fn one() -> Self;
}

impl<T: From<bool>> Zero for T {
    fn zero() -> Self {
        false.into()
    }
}

impl<T: From<bool>> One for T {
    fn one() -> Self {
        true.into()
    }
}

impl<T: One + Add<T, Output=T>> Increment for T {
    type Output = T;

    fn inc(self) -> Self::Output {
        self + T::one()
    }
}

impl<T: One + Sub<T, Output=T>> Decrement for T {
    type Output = T;

    fn dec(self) -> Self::Output {
        self - T::one()
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_isize() {
        assert_eq!(0, isize::zero());
        assert_eq!(1, isize::one());

        assert_eq!(2, isize::one().inc());
        assert_eq!(0, isize::one().dec());
    }

    #[test]
    fn test_usize() {
        assert_eq!(0, usize::zero());
        assert_eq!(1, usize::one());

        assert_eq!(2, usize::one().inc());
        assert_eq!(0, usize::one().dec());
    }

}