

use super::access::{Grid2DAccess, IGrid2DAccess};


pub struct InfiniGrid<G: Grid2DAccess> {
    grid: G,
}

impl<G: Grid2DAccess> InfiniGrid<G> {
    pub fn new(grid: G) -> InfiniGrid<G> {
        InfiniGrid { grid }
    }

    fn get_ucoor(&self, x: isize, y: isize) -> (usize, usize) {
        let (w, h) = self.grid.dimension();
        let ux = x.rem_euclid(w as isize) as usize;
        let uy = y.rem_euclid(h as isize) as usize;

        (ux, uy)
    }

    pub fn unwrap(self) -> G {
        self.grid
    }
}

impl<G: Grid2DAccess> IGrid2DAccess for InfiniGrid<G> {
    type Item = G::Item;

    fn get_i(&self, x: isize, y: isize) -> Option<&Self::Item> {
        let (x, y) = self.get_ucoor(x, y);
        self.grid.get(x, y)
    }

    fn origin(&self) -> (isize, isize) {
        (0, 0)
    }

    fn dimension(&self) -> (usize, usize) {
        (0, 0)
    }
}

