use std::{collections::HashMap, ops::Deref};

use crate::utils::{coor::{UCoor, ICoor}, graph::bfs, graph::dijkstra, direction::Direction};

use super::access::{Grid2DAccess, Grid2DAccessMut, IGrid2DAccess, IGrid2DAccessMut};


impl Grid2DAccess for Vec<Vec<char>> {
    type Item = char;

    fn get(&self, x: usize, y: usize) -> Option<&Self::Item> {
        self.deref().get(y).and_then(|row| row.get(x))
    }

    fn dimension(&self) -> (usize, usize) {
        let height = self.len();
        let width = self.first().unwrap().len();
        (width, height)
    }
}

impl Grid2DAccessMut for Vec<Vec<char>> {
    fn replace(&mut self, x: usize, y: usize, v: Self::Item) -> Option<Self::Item> {
        self.get_mut(y)
            .and_then(|row| row.get_mut(x))
            .map(|cell| std::mem::replace(cell, v))
    }
}

impl<T> Grid2DAccess for HashMap<UCoor, T> {
    type Item = T;

    fn get(&self, x: usize, y: usize) -> Option<&Self::Item> {
        self.get(&(x, y))
    }

    fn dimension(&self) -> (usize, usize) {
        let max_x = self.keys().map(|(_, x)| x).max().unwrap();
        let max_y = self.keys().map(|(y, _)| y).max().unwrap();

        (*max_x, *max_y)
    }
}

impl<T> Grid2DAccessMut for HashMap<UCoor, T> {
    fn replace(&mut self, x: usize, y: usize, v: Self::Item) -> Option<Self::Item> {
        self.insert((x, y), v)
    }
}

impl<T> IGrid2DAccess for HashMap<ICoor, T> {
    type Item = T;

    fn get_i(&self, x: isize, y: isize) -> Option<&Self::Item> {
        self.get(&(x, y))
    }

    fn origin(&self) -> (isize, isize) {
        let min_x = *self.keys().map(|(x, _)| x).min().unwrap_or(&0);
        let min_y = *self.keys().map(|(_, y)| y).min().unwrap_or(&0);

        (min_x, min_y)
    }

    fn dimension(&self) -> (usize, usize) {
        let (ox, oy) = self.origin();
        let (bx, by) = self.boundary();

        (ox.abs_diff(bx), oy.abs_diff(by))
    }

    fn boundary(&self) -> (isize, isize) {
        let max_x = *self.keys().map(|(x, _)| x).max().unwrap_or(&0);
        let max_y = *self.keys().map(|(_, y)| y).max().unwrap_or(&0);

        (max_x, max_y)
    }
}

impl<T> IGrid2DAccessMut for HashMap<ICoor, T> {
    fn replace_i(&mut self, x: isize, y: isize, v: Self::Item) -> Option<Self::Item> {
        self.insert((x, y), v)
    }
}

impl<G: Grid2DAccess> bfs::Graph for G {
    type VertexId = UCoor;

    fn neighbors(&self, vertex: &Self::VertexId) -> Vec<Self::VertexId> {
        Direction::all()
            .into_iter()
            .filter_map(|v| v.try_walk(vertex))
            .collect()
    }
}

impl<G: Grid2DAccess> dijkstra::Graph for G 
where G::Item: Clone {
    type VertexId = UCoor;
    type Cost = G::Item;

    fn neighbors(&self, vertex: &Self::VertexId) -> Vec<(Self::VertexId, Self::Cost)> {
        Direction::all()
            .into_iter()
            .filter_map(|v| v.try_walk(vertex))
            .map(|dest| (dest, self.get_coor(&dest).unwrap().to_owned()))
            .collect()
    }
}
