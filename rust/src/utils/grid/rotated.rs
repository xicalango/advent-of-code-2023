use std::fmt::Debug;
use crate::utils::direction::Direction;

use super::access::{Grid2DAccess, Grid2DAccessMut};


macro_rules! rotated_view_impl {
    ($t:ident) => {
        impl<'a, A> $t<'a, A>
        where
            A: Grid2DAccess,
        {
            fn translate_xy(&self, x: usize, y: usize) -> (usize, usize) {
                let (w, h) = self.grid.dimension();

                match self.rotation {
                    Direction::North => (x, y),
                    Direction::East => (w - y - 1, x),
                    Direction::South => (w - x - 1, h - y - 1),
                    Direction::West => (y, h - x - 1),
                }
            }
        }

        impl<'a, A> Grid2DAccess for $t<'a, A>
        where
            A: Grid2DAccess,
        {
            type Item = A::Item;

            fn get(&self, x: usize, y: usize) -> Option<&A::Item> {
                let (x, y) = self.translate_xy(x, y);
                self.grid.get(x, y)
            }

            fn dimension(&self) -> (usize, usize) {
                match self.rotation {
                    Direction::North | Direction::South => (self.grid.width(), self.grid.height()),
                    Direction::East | Direction::West => (self.grid.height(), self.grid.width()),
                }
            }
        }

        impl<'a, A: Debug + Grid2DAccess> Debug for $t<'a, A> {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                f.debug_struct(stringify!($t))
                    .field("grid", &self.grid)
                    .field("rotation", &self.rotation)
                    .finish()
            }
        }
    };
}

pub struct RotatedView<'a, A: Grid2DAccess> {
    grid: &'a A,
    rotation: Direction,
}

impl<'a, A: Grid2DAccess> RotatedView<'a, A> {
    pub fn new(grid: &'a A, rotation: Direction) -> RotatedView<'a, A> {
        RotatedView { grid, rotation }
    }
}

rotated_view_impl!(RotatedView);

pub struct RotatedViewMut<'a, A: Grid2DAccess> {
    grid: &'a mut A,
    rotation: Direction,
}

rotated_view_impl!(RotatedViewMut);

impl<'a, A: Grid2DAccess> RotatedViewMut<'a, A> {
    pub fn new(grid: &'a mut A, rotation: Direction) -> RotatedViewMut<'a, A> {
        RotatedViewMut { grid, rotation }
    }
}

impl<'a, A: Grid2DAccessMut> Grid2DAccessMut for RotatedViewMut<'a, A> {
    fn replace(&mut self, x: usize, y: usize, v: Self::Item) -> Option<Self::Item> {
        let (x, y) = self.translate_xy(x, y);
        self.grid.replace(x, y, v)
    }
}

