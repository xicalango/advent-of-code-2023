use std::fmt::{Debug, Display};
use std::hash::Hash;

use crate::utils::direction::Direction;

use super::{access::Grid2DAccess, access::Grid2DAccessMut, rotated::{RotatedView, RotatedViewMut}};


pub struct Grid2D<T> {
    grid: Vec<T>,
    dimension: (usize, usize),
}

impl<T: Clone> Grid2D<T> {
    pub fn new(w: usize, h: usize, v: T) -> Grid2D<T> {
        let grid = vec![v; w * h];
        let dimension = (w, h);
        Grid2D { grid, dimension }
    }

    pub fn new_with<F: Fn(usize, usize) -> T>(w: usize, h: usize, f: F) -> Grid2D<T> {
        let mut grid = Vec::new();
        grid.reserve_exact(w * h);
        for y in 0..h {
            for x in 0..w {
                let e = f(x, y);
                grid.push(e);
            }
        }
        Grid2D {
            grid,
            dimension: (w, h),
        }
    }

    pub fn new_from_raw(raw_grid: Vec<T>, width: usize, height: usize) -> Grid2D<T> {
        assert_eq!(raw_grid.len(), width * height);
        Grid2D {
            grid: raw_grid,
            dimension: (width, height),
        }
    }
}

impl<T: Clone> Clone for Grid2D<T> {
    fn clone(&self) -> Self {
        Self {
            grid: self.grid.clone(),
            dimension: self.dimension.clone(),
        }
    }
}

impl<T> Grid2D<T> {
    pub fn from_grid_access(ga: &impl Grid2DAccess<Item = T>) -> Grid2D<&T> {
        let mut result = Vec::new();
        for row in ga.rows() {
            for col in row {
                result.push(col);
            }
        }

        Grid2D {
            grid: result,
            dimension: ga.dimension(),
        }
    }

    fn index(&self, x: usize, y: usize) -> Option<usize> {
        if x >= self.width() || y >= self.height() {
            return None;
        }

        Some(y * self.width() + x)
    }

    pub fn rotated(&self, rotation: Direction) -> RotatedView<Self> {
        RotatedView::new(self, rotation)
    }

    pub fn rotated_mut(&mut self, rotation: Direction) -> RotatedViewMut<Self> {
        RotatedViewMut::new(self, rotation)
    }
}

impl<T: Debug> Debug for Grid2D<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Grid2D")
            .field("grid", &self.grid)
            .field("dimension", &self.dimension)
            .finish()
    }
}

impl<T: Display> Display for Grid2D<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for y in 0..self.height() {
            for x in 0..self.width() {
                write!(f, "{}", self.get(x, y).unwrap())?;
            }
            writeln!(f, "")?;
        }
        Ok(())
    }
}

impl<T: PartialEq> PartialEq for Grid2D<T> {
    fn eq(&self, other: &Self) -> bool {
        self.grid == other.grid && self.dimension == other.dimension
    }
}

impl<T: Eq> Eq for Grid2D<T> {}

impl<T: Hash> Hash for Grid2D<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.grid.hash(state);
        self.dimension.hash(state);
    }
}

impl<T> Grid2DAccess for Grid2D<T> {
    type Item = T;

    fn get(&self, x: usize, y: usize) -> Option<&T> {
        self.index(x, y).and_then(|i| self.grid.get(i))
    }

    fn dimension(&self) -> (usize, usize) {
        self.dimension.to_owned()
    }
}

impl<T> Grid2DAccessMut for Grid2D<T> {
    fn replace(&mut self, x: usize, y: usize, v: T) -> Option<T> {
        self.index(x, y)
            .map(|i| std::mem::replace(&mut self.grid[i], v))
    }
}

impl<T> From<Vec<Vec<T>>> for Grid2D<T> {
    fn from(value: Vec<Vec<T>>) -> Self {
        let h = value.len();
        let w = value.first().unwrap().len();

        let mut ts: Vec<T> = Vec::new();
        ts.reserve(h * w);

        for l in value {
            for v in l {
                ts.push(v);
            }
        }

        Grid2D {
            grid: ts,
            dimension: (w, h),
        }
    }
}

impl<T> FromIterator<Vec<T>> for Grid2D<T> {
    fn from_iter<I: IntoIterator<Item = Vec<T>>>(iter: I) -> Self {
        let vec: Vec<Vec<T>> = iter.into_iter().collect();
        vec.into()
    }
}

