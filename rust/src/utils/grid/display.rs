use std::fmt::{Debug, Display};

use super::access::Grid2DAccess;

pub struct GridDisplay<'a, D: Grid2DAccess> {
    grid: &'a D,
}

impl<'a, D: Grid2DAccess> GridDisplay<'a, D> {
    pub fn new(grid: &'a D) -> GridDisplay<D> {
        GridDisplay { grid }
    }
}

impl<'a, D: Grid2DAccess> Display for GridDisplay<'a, D>
where
    D::Item: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in self.grid.rows() {
            for symbol in row {
                write!(f, "{symbol}")?;
            }
            writeln!(f, "")?;
        }
        Ok(())
    }
}

impl<'a, D: Grid2DAccess> Debug for GridDisplay<'a, D>
where
    D::Item: Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in self.grid.rows() {
            for symbol in row {
                write!(f, "{symbol:?}")?;
            }
            writeln!(f, "")?;
        }
        Ok(())
    }
}

pub trait XpmDisplay {

    fn character_colors() -> Vec<(char, String)>;
    fn xpm_char(&self) -> char;

}

pub struct XpmGridDisplay<'a, D: Grid2DAccess>(&'a D);

impl<'a, D: Grid2DAccess> XpmGridDisplay<'a, D> {
    pub fn new(grid: &'a D) -> XpmGridDisplay<D> {
        XpmGridDisplay(grid)
    }
}

impl<'a, D: Grid2DAccess> Display for XpmGridDisplay<'a, D>
where D::Item: XpmDisplay {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let XpmGridDisplay(grid) = self;

        let character_colors = D::Item::character_colors();

        writeln!(f, "/* XPM */")?;
        writeln!(f, "static char * XFACE[] = {{")?;
        writeln!(f, "\"{} {} {} {}\"", grid.width(), grid.height(), character_colors.len(), 1)?;
        for (char, color) in character_colors {
            writeln!(f, "\"{char} c {color}\"")?;
        }
        for row in grid.rows() {
            let is_last_row = row.is_last();

            write!(f, "\"")?;
            for symbol in row {
                write!(f, "{}", symbol.xpm_char())?;
            }
            write!(f, "\"")?;

            if !is_last_row {
                write!(f, ",")?;
            }
            writeln!(f, "")?;
        }
        writeln!(f, "}};")?;

        Ok(())
    }
}
