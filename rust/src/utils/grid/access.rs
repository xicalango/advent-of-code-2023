use std::borrow::Borrow;

use crate::utils::coor::{UCoor, ICoor};

use super::{display::GridDisplay, XpmGridDisplay, XpmDisplay};


pub trait Grid2DAccess {
    type Item;

    fn get(&self, x: usize, y: usize) -> Option<&Self::Item>;

    fn dimension(&self) -> UCoor;

    fn get_coor<T: Borrow<UCoor>>(&self, c: T) -> Option<&Self::Item> {
        let (x, y) = c.borrow();
        self.get(*x, *y)
    }

    fn width(&self) -> usize {
        self.dimension().0
    }

    fn height(&self) -> usize {
        self.dimension().1
    }

    fn rows(&self) -> Rows<'_, Self>
    where
        Self: Sized,
    {
        Rows::new(self)
    }

    fn all_elements(&self) -> AllElements<'_, Self>
    where
        Self: Sized,
    {
        AllElements {
            grid: self,
            x: 0,
            y: 0,
            width: self.width(),
        }
    }

    fn sparse_rows(&self) -> SparseRows<'_, Self>
    where
        Self: Sized,
    {
        SparseRows::new(self)
    }

    fn sparse_all_elements(&self) -> SparseAllElements<'_, Self>
    where
        Self: Sized,
    {
        SparseAllElements {
            grid: self,
            x: 0,
            y: 0,
            width: self.width(),
            height: self.height(),
        }
    }

    fn display(&self) -> GridDisplay<'_, Self>
    where
        Self: Sized,
    {
        GridDisplay::new(self)
    }

    fn xpm_display(&self) -> XpmGridDisplay<'_, Self>
    where
        Self: Sized,
        Self::Item: XpmDisplay
    {
        XpmGridDisplay::new(self)
    }

    fn find<P>(&self, mut predicate: P) -> Option<(usize, usize)>
    where
        P: FnMut(&Self::Item) -> bool,
        Self: Sized,
    {
        for (y, row) in self.rows().enumerate() {
            if let Some(x) = row.enumerate().find(|(_, e)| predicate(e)).map(|(x, _)| x) {
                return Some((x, y));
            }
        }
        return None;
    }
}

pub trait Grid2DAccessMut: Grid2DAccess {
    fn replace(&mut self, x: usize, y: usize, v: Self::Item) -> Option<Self::Item>;

    fn replace_coor<T: Borrow<(usize, usize)>>(
        &mut self,
        c: T,
        v: Self::Item,
    ) -> Option<Self::Item> {
        let (x, y) = c.borrow();
        self.replace(*x, *y, v)
    }
}

pub struct AllElements<'a, A: Grid2DAccess> {
    grid: &'a A,
    x: usize,
    y: usize,
    width: usize,
}

impl<'a, A: Grid2DAccess> Iterator for AllElements<'a, A> {
    type Item = &'a A::Item;

    fn next(&mut self) -> Option<Self::Item> {
        let e = self.grid.get(self.x, self.y);

        self.x += 1;
        if self.x >= self.width {
            self.x = 0;
            self.y += 1;
        }

        e
    }
}

pub struct Row<'a, A: Grid2DAccess> {
    grid: &'a A,
    y: usize,
    is_last: bool,
    x: usize,
    width: usize,
}

impl<'a, A: Grid2DAccess> Row<'a, A> {
    pub fn new(grid: &'a A, y: usize, is_last: bool) -> Row<'a, A> {
        Row {
            grid,
            y,
            is_last,
            x: 0,
            width: grid.width(),
        }
    }

    pub fn is_last(&self) -> bool {
        self.is_last
    }
}

impl<'a, A: Grid2DAccess> Iterator for Row<'a, A> {
    type Item = &'a A::Item;

    fn next(&mut self) -> Option<Self::Item> {
        if self.x >= self.width {
            None
        } else {
            let e = self.grid.get(self.x, self.y);
            self.x += 1;
            e
        }
    }
}

pub struct Rows<'a, A: Grid2DAccess> {
    grid: &'a A,
    y: usize,
    height: usize,
}

impl<'a, A: Grid2DAccess> Rows<'a, A> {
    pub fn new(grid: &'a A) -> Rows<'a, A> {
        Rows {
            grid,
            y: 0,
            height: grid.height(),
        }
    }
}

impl<'a, A: Grid2DAccess> Iterator for Rows<'a, A> {
    type Item = Row<'a, A>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.y >= self.height {
            None
        } else {
            let e = Some(Row::new(self.grid, self.y, self.y == self.height - 1));
            self.y += 1;
            e
        }
    }
}

pub struct SparseAllElements<'a, A: Grid2DAccess> {
    grid: &'a A,
    x: usize,
    y: usize,
    width: usize,
    height: usize,
}

impl<'a, A: Grid2DAccess> Iterator for SparseAllElements<'a, A> {
    type Item = Option<&'a A::Item>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.y >= self.height {
            return None;
        }

        let e = self.grid.get(self.x, self.y);

        self.x += 1;
        if self.x >= self.width {
            self.x = 0;
            self.y += 1;
        }

        Some(e)
    }
}

pub struct SparseRow<'a, A: Grid2DAccess> {
    grid: &'a A,
    y: usize,
    x: usize,
    width: usize,
}

impl<'a, A: Grid2DAccess> SparseRow<'a, A> {
    pub fn new(grid: &'a A, y: usize) -> SparseRow<A> {
        SparseRow {
            grid,
            y,
            x: 0,
            width: grid.width(),
        }
    }
}

impl<'a, A: Grid2DAccess> Iterator for SparseRow<'a, A> {
    type Item = Option<&'a A::Item>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.x >= self.width {
            None
        } else {
            let e = self.grid.get(self.x, self.y);
            self.x += 1;
            Some(e)
        }
    }
}

pub struct SparseRows<'a, A: Grid2DAccess> {
    grid: &'a A,
    y: usize,
    height: usize,
}

impl<'a, A: Grid2DAccess> SparseRows<'a, A> {
    pub fn new(grid: &'a A) -> SparseRows<'a, A> {
        SparseRows {
            grid,
            y: 0,
            height: grid.height(),
        }
    }
}

impl<'a, A: Grid2DAccess> Iterator for SparseRows<'a, A> {
    type Item = SparseRow<'a, A>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.y >= self.height {
            None
        } else {
            let e = Some(SparseRow::new(self.grid, self.y));
            self.y += 1;
            e
        }
    }
}

pub trait IGrid2DAccess {
    type Item;

    fn get_i(&self, x: isize, y: isize) -> Option<&Self::Item>;
    fn get_icoor<C: Borrow<ICoor>>(&self, coor: C) -> Option<&Self::Item> {
        let (x, y) = *coor.borrow();
        self.get_i(x, y)
    }

    fn origin(&self) -> (isize, isize);
    fn dimension(&self) -> (usize, usize);

    fn boundary(&self) -> (isize, isize) {
        let (ox, oy) = self.origin();
        let (w, h) = self.dimension();

        (
            ox.checked_add_unsigned(w).unwrap(),
            oy.checked_add_unsigned(h).unwrap(),
        )
    }
}

pub trait IGrid2DAccessMut: IGrid2DAccess {
    fn replace_i(&mut self, x: isize, y: isize, v: Self::Item) -> Option<Self::Item>;

    fn replace_icoor(&mut self, coor: ICoor, v: Self::Item) -> Option<Self::Item> {
        let (x, y) = coor;
        self.replace_i(x, y, v)
    }
}
