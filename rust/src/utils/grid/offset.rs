use std::fmt::Debug;

use crate::utils::coor::UCoor;

use super::access::{Grid2DAccess, IGrid2DAccess, Grid2DAccessMut, IGrid2DAccessMut};


pub struct OffsetGrid<G: Grid2DAccess> {
    offset: (isize, isize),
    grid: G,
}

impl<G: Grid2DAccess> OffsetGrid<G> {
    pub fn new(offset: (isize, isize), grid: G) -> OffsetGrid<G> {
        OffsetGrid { offset, grid }
    }

    fn get_ucoor(&self, x: isize, y: isize) -> UCoor {
        let (ox, oy) = self.offset;
        let x = (x - ox) as usize;
        let y = (y - oy) as usize;
        (x, y)
    }

    pub fn unwrap(self) -> G {
        self.grid
    }
}
impl<G: Grid2DAccess> IGrid2DAccess for OffsetGrid<G> {
    type Item = G::Item;
    fn get_i(&self, x: isize, y: isize) -> Option<&Self::Item> {
        let (x, y) = self.get_ucoor(x, y);
        self.grid.get(x, y)
    }
    fn origin(&self) -> (isize, isize) {
        self.offset.to_owned()
    }
    fn dimension(&self) -> (usize, usize) {
        self.grid.dimension()
    }
}

impl<G: Grid2DAccess> Debug for OffsetGrid<G>
where G: Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("OffsetGrid").field("offset", &self.offset).field("grid", &self.grid).finish()
    }
}

impl<G: Grid2DAccessMut> IGrid2DAccessMut for OffsetGrid<G> {
    fn replace_i(&mut self, x: isize, y: isize, v: Self::Item) -> Option<Self::Item> {
        let (x, y) = self.get_ucoor(x, y);
        self.grid.replace(x, y, v)
    }
}

impl<G: Grid2DAccess> Grid2DAccess for OffsetGrid<G> {
    type Item = G::Item;

    fn get(&self, x: usize, y: usize) -> Option<&Self::Item> {
        self.grid.get(x, y)
    }

    fn dimension(&self) -> UCoor {
        self.grid.dimension()
    }
}

impl<G: Grid2DAccessMut> Grid2DAccessMut for OffsetGrid<G> {
    fn replace(&mut self, x: usize, y: usize, v: Self::Item) -> Option<Self::Item> {
        self.grid.replace(x, y, v)
    }
}

