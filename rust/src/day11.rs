use std::str::FromStr;
use anyhow::{Result, Error};

type Coor = (usize, usize);

#[derive(Debug)]
pub struct Map {
    galaxies: Vec<Coor>,
    expanded_rows: Vec<usize>,
    expanded_cols: Vec<usize>,
}

impl FromStr for Map {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut galaxies = Vec::new();
        let mut expanded_rows = Vec::new();
        let mut galaxy_col_counter = Vec::new();


        for (y, line) in s.lines().enumerate() {
            let line = line.trim_end();
            if galaxy_col_counter.len() == 0 {
                galaxy_col_counter.resize(line.len(), 0);
            }

            let mut row_has_galaxy = false;
            for (x, _) in line.char_indices().filter(|(_, c)| c == &'#') {
                row_has_galaxy = true;
                galaxy_col_counter[x] += 1;
                galaxies.push((x, y));
                if cfg!(any(test, feature = "debug")) {
                    println!("found galaxy {} at {:?}", galaxies.len(), (x, y));
                }
            }

            if !row_has_galaxy {
                expanded_rows.push(y);
            }
        }

        let expanded_cols: Vec<usize> = galaxy_col_counter.iter().enumerate().filter(|(_, c)| c == &&0).map(|(i, _)| i).collect();

        Ok(Map {
           galaxies,
           expanded_rows,
           expanded_cols,
        })
    }
}

impl Map {

    fn distance<const EXPANSE: usize>(&self, g1: usize, g2: usize) -> usize {
        let (x1, y1) = &self.galaxies[g1];
        let (x2, y2) = &self.galaxies[g2];

        let xr = x1.min(x2)..=x1.max(x2);
        let yr = y1.min(y2)..=y1.max(y2);

        let x_expansions = self.expanded_cols.iter().filter(|r| xr.contains(r)).count();
        let y_expansions = self.expanded_rows.iter().filter(|r| yr.contains(r)).count();

        let expanse = EXPANSE - 1;

        x1.abs_diff(*x2) + y1.abs_diff(*y2) + (x_expansions * expanse) + (y_expansions * expanse)
    }

    pub fn solution<const EXPANSE: usize>(&self) -> usize {
        let mut result = 0;

        for i in 0..self.galaxies.len() {
            for j in i..self.galaxies.len() {
                result += self.distance::<EXPANSE>(i, j);
            }
        }

        result
    }

}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day11/example.txt");

    #[test]
    fn test_s1_examples() {
        let map: Map = INPUT.parse().unwrap();
        for (sid, did, len) in [(5, 9, 9), (1, 7, 15), (3, 6, 17), (8, 9, 5)] {
            println!("TTT from {sid} to {did} should be {len}");
            assert_eq!(map.distance::<2>(sid - 1, did - 1), len);
        }
    }

    #[test]
    fn test_s1() {
        let map: Map = INPUT.parse().unwrap();
        let s = map.solution::<2>();
        assert_eq!(s, 374);
    }

    #[test]
    fn test_s2() {
        let map: Map = INPUT.parse().unwrap();
        let s = map.solution::<10>();
        assert_eq!(s, 1030);
        let s = map.solution::<100>();
        assert_eq!(s, 8410);
    }

}
