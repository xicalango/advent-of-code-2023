use std::{collections::{HashMap, HashSet}, str::FromStr};

type XY = (usize, usize);

#[derive(Debug)]
pub struct Manual {
    number_map: HashMap<XY, usize>,
    part_map: Vec<usize>,
    read_map: HashMap<XY, char>,
}

impl FromStr for Manual {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut number_map: HashMap<XY, usize> = HashMap::new();
        let mut part_map: Vec<usize> = Vec::new();
        let mut read_map: HashMap<XY, char> = HashMap::new();

        for (y, line) in s.lines().enumerate() {
            let mut cur = String::new();

            for (x, c) in line.char_indices() {
                if c.is_ascii_digit() {
                    number_map.insert((x, y), part_map.len());
                    cur.push(c);
                } else {
                    if cur.len() > 0 {
                        let number: usize = cur.parse().unwrap();
                        part_map.push(number);
                        cur.clear();
                    }

                    if c != '.' {
                        read_map.insert((x, y), c);
                    }
                }
            }
            if cur.len() > 0 {
                let number: usize = cur.parse().unwrap();
                part_map.push(number);
                cur.clear();
            }
        }

        Ok(Manual {
            number_map,
            part_map,
            read_map,
        })
    }
}

static SURROUNDINGS: [isize; 3] = [-1, 0, 1];

impl Manual {
    pub fn solution1(&self) -> usize {
        let mut parts: HashSet<usize> = HashSet::new();

        for &(x, y) in self.read_map.keys() {
            for &xd in SURROUNDINGS.iter() {
                for &yd in SURROUNDINGS.iter() {
                    let cx = x.saturating_add_signed(xd);
                    let cy = y.saturating_add_signed(yd);
                    if let Some(part_id) = self.number_map.get(&(cx, cy)) {
                        parts.insert(*part_id);
                    }
                }
            }
        }

        parts.into_iter().map(|v| self.part_map[v]).sum()
    }

    pub fn solution2(&self) -> usize {
        let mut parts: HashSet<(usize, usize)> = HashSet::new();

        for &(x, y) in self.read_map.iter()
                .filter(|(_, &c)| c == '*')
                .map(|(c, _)| c) {

            let mut neighbors = HashSet::new();

            for &xd in SURROUNDINGS.iter() {
                for &yd in SURROUNDINGS.iter() {
                    let cx = x.saturating_add_signed(xd);
                    let cy = y.saturating_add_signed(yd);
                    if let Some(part_id) = self.number_map.get(&(cx, cy)) {
                        neighbors.insert(*part_id);
                    }
                }
            }

            if neighbors.len() == 2 {
                let neighbors: Vec<usize> = neighbors.into_iter().collect();
                parts.insert((neighbors[0], neighbors[1]));
            }
        }

        parts.into_iter().map(|(p1, p2)| {
            let v1 = self.part_map[p1];
            let v2 = self.part_map[p2];
            v1 * v2
        }).sum()
    }

    pub fn combined_solutions(&self) -> (usize, usize) {
        let mut parts_1: HashSet<usize> = HashSet::new();
        let mut parts_2: HashSet<(usize, usize)> = HashSet::new();

        let mut sol_1 = 0 as usize;
        let mut sol_2 = 0 as usize;

        for (&(x, y), &c) in self.read_map.iter() {
            let mut neighbors = HashSet::new();

            for &xd in SURROUNDINGS.iter() {
                for &yd in SURROUNDINGS.iter() {
                    let cx = x.saturating_add_signed(xd);
                    let cy = y.saturating_add_signed(yd);
                    if let Some(&part_id) = self.number_map.get(&(cx, cy)) {
                        if !parts_1.contains(&part_id) {
                            parts_1.insert(part_id);
                            sol_1 += self.part_map[part_id];
                        }
                        if c == '*' && neighbors.len() < 3 {
                            neighbors.insert(part_id);
                        }
                    }
                }
            }

            if c == '*' && neighbors.len() == 2 {
                let neighbors: Vec<usize> = neighbors.into_iter().collect();
                let neighbors = (neighbors[0], neighbors[1]);
                if !parts_2.contains(&neighbors) {
                    sol_2 += self.part_map[neighbors.0] * self.part_map[neighbors.1];
                    parts_2.insert(neighbors);
                }
            }

        }

        (sol_1, sol_2)
    }

}

#[allow(dead_code)]
pub fn solution1(input: &str) -> usize {
    let manual: Manual = input.parse().unwrap();
    manual.solution1()
}

#[allow(dead_code)]
pub fn solution2(input: &str) -> usize {
    let manual: Manual = input.parse().unwrap();
    manual.solution2()
}

#[allow(dead_code)]
pub fn combined_solutions(input: &str) -> (usize, usize) {
    let manual: Manual = input.parse().unwrap();
    manual.combined_solutions()
}

#[cfg(test)] 
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day3/example.txt");

    #[test]
    fn test_example1() {
        let s = solution1(INPUT);
        println!("{s}");
        assert_eq!(s, 4361);
    }

    #[test]
    fn test_example2() {
        let s = solution2(INPUT);
        println!("{s}");
        assert_eq!(s, 467835);
    }
    
    #[test]
    fn test_combined() {
        let (s1, s2) = combined_solutions(INPUT);
        println!("{s1}, {s2}");
        assert_eq!(s1, 4361);
        assert_eq!(s2, 467835);
    }

    #[test]
    fn test_example2_combined() {
        let input = include_str!("../../data/day3/example2.txt");
        let (s1, s2) = combined_solutions(input);
        println!("{s1}, {s2}");
        assert_eq!(s1, 925);
        assert_eq!(s2, 6756);
    }
}
