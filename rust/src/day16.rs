
use std::{str::FromStr, fmt::Display, collections::HashSet};

use anyhow::{Result, Error, anyhow};

use crate::utils::{grid::{Grid2D, Grid2DAccess, Grid2DAccessMut}, direction::Direction, coor::{ICoor, UCoor}};

use Tile::*;
use Direction::*;

#[derive(Debug, PartialEq, Eq)]
enum Tile {
    Empty,
    Splitter(Direction, Direction),
    Mirror(Direction, Direction),
}

impl TryFrom<char> for Tile {
    type Error = Error;

    fn try_from(value: char) -> Result<Self, Self::Error> {

        match value {
            '.' => Ok(Empty),
            '-' => Ok(Splitter(East, West)),
            '|' => Ok(Splitter(North, South)),
            '/' => Ok(Mirror(West, North)),
            '\\' => Ok(Mirror(West, South)),
            e => Err(anyhow!("invalid tile: {e}")),
        }
    }
}

impl Display for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let c = match self {
            Empty => '.',
            Splitter(East, West) => '─',
            Splitter(North, South) => '│',
            Mirror(West, North) => '╱',
            Mirror(West, South) => '╲',
            _ => panic!(),
        };

        write!(f, "{c}")
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Beam {
    position: ICoor,
    direction: Direction,
}

impl Beam {

    fn new(x: isize, y: isize, direction: Direction) -> Beam {
        Beam { position: (x, y), direction }
    }

    fn advance(&mut self) {
        self.position = self.direction.walk(&mut self.position);
    }

    fn get_uposition(&self) -> Option<UCoor> {
        let (x, y) = self.position;
        if x < 0 || y < 0 {
            None
        } else {
            Some((x as usize, y as usize))
        }
    }

}

#[derive(Debug)]
struct Contraption {
    grid: Grid2D<Tile>,
}

impl FromStr for Contraption {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let tiles: Result<Vec<Vec<Tile>>> = s.lines()
            .map(|line| line.chars()
                .map(|c| c.try_into())
                .collect::<Result<Vec<Tile>>>()
            ).collect();

        let grid: Grid2D<Tile> = tiles?.into();

        Ok(Contraption {
            grid,
        })
    }
}

impl Contraption {

    fn shmovement(&self, start_beam: Beam) -> Grid2D<bool> {
        let mut visited_locations: HashSet<Beam> = HashSet::new();

        let mut result = Grid2D::new(self.grid.width(), self.grid.height(), false);
        let mut beams = vec![start_beam];

        while let Some(mut beam) = beams.pop() {
            if cfg!(feature = "debug") {
                println!("{:?}", beam);
                println!("{}", result);
                println!();
            }

            if visited_locations.contains(&beam) {
                continue;
            }

            visited_locations.insert(beam.clone());

            if let Some(pos) = beam.get_uposition() {
                result.replace_coor(&pos, true);
            }

            beam.advance();

            let pos = beam.get_uposition();
            if pos.is_none() {
                continue;
            }

            let pos = pos.unwrap(); 

            match self.grid.get_coor(&pos) {
                Some(Splitter(d1, d2)) if &beam.direction != d1 && &beam.direction != d2 => {
                    if cfg!(feature = "debug") {
                        println!("encountered {d1:?} {d2:?} splitter from {:?} -> split", beam.direction);
                    }
                    beam.direction = *d1;
                    beams.push(Beam { position: beam.position, direction: *d2 });
                    beams.push(beam);
                },
                Some(Mirror(d1, d2)) => {
                    let arriving_direction = beam.direction.invert();

                    if cfg!(feature = "debug") {
                        println!("encountered {d1:?} {d2:?} mirror from {:?}", arriving_direction);
                    }
                    let di1 = d1.invert();
                    let di2 = d2.invert();

                    if &arriving_direction == d1 {
                        beam.direction = *d2;
                    } else if &arriving_direction == d2 {
                        beam.direction = *d1;
                    } else if arriving_direction == di1 {
                        beam.direction = di2;
                    } else if arriving_direction == di2 {
                        beam.direction = di1;
                    } else {
                        panic!()
                    }

                    if cfg!(feature = "debug") {
                        println!("encountered {d1:?} {d2:?} mirror to {:?}", beam.direction);
                    }

                    beams.push(beam);
                },
                Some(s) => {
                    if cfg!(feature = "debug") {
                        println!("encountered {s:?}. nothing happens");
                    }
                    beams.push(beam);
                },
                None => {
                    // beam out of bounds
                },
            }
            
            if cfg!(feature = "debug") {
                println!("{:?}", beams);
            }
        }

        result
    }

    fn count_energized_tiles(&self, beam: Beam) -> usize {
        let tiles = self.shmovement(beam);
        tiles.all_elements().filter(|&&c| c).count()
    }

}

pub fn solution1(input: &str) -> usize {
    let c: Contraption = input.parse().unwrap();
    if cfg!(feature = "pretty") {
        println!("{}", c.grid);
    }
    c.count_energized_tiles(Beam::new(-1, 0, Direction::East))
}

pub fn solution2(input: &str) -> usize {
    let c: Contraption = input.parse().unwrap();

    Direction::all().iter().map(|dir| {
        let coor: ICoor = (0, 0);
        let (x, y) = dir.invert().walk(&coor);

        let bound = match dir {
            North | South => c.grid.width() as isize,
            East | West => c.grid.height() as isize,
        };

        (0..bound).map(|b| {
            let (bx, by) = match dir {
                North | South => (b, y),
                East | West => (x, b),
            };

            let beam = Beam::new(bx, by, *dir);

            c.count_energized_tiles(beam)
        }).max().unwrap()

    }).max().unwrap()
}

#[cfg(test)]
mod test {
    use crate::utils::direction::Direction;

    use super::*;


    const INPUT: &'static str = include_str!("../../data/day16/example.txt");

    #[test]
    fn test_grid() {
        let contraption: Contraption = INPUT.parse().unwrap();
        println!("{}", contraption.grid);

        let beam = Beam::new(-1, 0, Direction::East);

        let visited_grid = contraption.shmovement(beam);

        println!("{}", visited_grid);
    }

    #[test]
    fn test_s1() {
        let s = solution1(INPUT);
        assert_eq!(s, 46);
    }

    #[test]
    fn test_s2() {
        let s = solution2(INPUT);
        assert_eq!(s, 51);
    }

}