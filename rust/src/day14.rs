use std::{str::FromStr, fmt::{Debug, Display}, hash::Hash, collections::HashMap, rc::Rc};
use anyhow::{Error, Result};

use crate::utils::{direction::Direction, grid::{Grid2D, Grid2DAccess, Grid2DAccessMut}};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct ReflectorDish {
    grid: Grid2D<char>,
}

impl FromStr for ReflectorDish {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let grid: Vec<Vec<char>> = s.lines().map(str::trim_end).map(|s| s.chars().collect()).collect();
        Ok(ReflectorDish { grid: grid.into() })
    }
}

impl Display for ReflectorDish {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.grid)
    }
}

impl ReflectorDish {

    fn calc_load(&self) -> usize {
        self.grid.rows().enumerate().map(|(y, row)| {
            (self.grid.height() - y) * row.filter(|c| c == &&'O').count()
        }).sum()
    }

    fn shift(&self, direction: Direction) -> ReflectorDish {
        let grid_view = self.grid.rotated(direction);

        let mut new_grid = Grid2D::new(self.grid.width(), self.grid.height(), '.');
        let mut new_grid_view = new_grid.rotated_mut(direction);

        let mut fall_index: Vec<usize> = vec![0; grid_view.width()];

        for (y, row) in grid_view.rows().enumerate() {
            for (x, c) in row.enumerate() {
                match c {
                    '#' => {
                        new_grid_view.replace(x, y, '#');
                        fall_index[x] = y + 1;
                    },
                    'O' => {
                        if fall_index[x] >= y {
                            new_grid_view.replace(x, y, 'O');
                            fall_index[x] = y + 1;
                        } else {
                            new_grid_view.replace(x, fall_index[x], 'O');
                            fall_index[x] += 1;
                        }
                    },
                    '.' => {
                        // don't need to fill anything, it's already '.'
                    },
                    _ => panic!("What are thooose?"),
                        
                }
            }
        }

        ReflectorDish { grid: new_grid }

    }

}

pub fn solution1(input: &str) -> usize {

    let mut grid: Vec<String> = Vec::new();
    let mut fall_index: Vec<usize> = Vec::new();

    for (y, line) in input.lines().map(str::trim_end).enumerate() {
        if fall_index.len() < line.len() {
            fall_index.resize(line.len(), 0);
        }

        let mut current_row = String::new();
        current_row.reserve(line.len());

        for (x, c) in line.char_indices() {

            match c {
                '.' => {
                    current_row.push('.');
                },
                '#' => {
                    current_row.push('#');
                    fall_index[x] = y + 1;
                },
                'O' => {
                    if fall_index[x] >= y {
                        current_row.push('O');
                        fall_index[x] = y + 1;
                    } else {
                        current_row.push('.');
                        grid[fall_index[x]].replace_range(x..x+1, "O");
                        fall_index[x] += 1;
                    }
                },
                _ => panic!("What are thooose?"),
            }

        }

        grid.push(current_row);
    }

    grid.iter()
        .enumerate()
        .map(|(i, v)| (grid.len() - i) * v.chars().filter(|c| c == &'O').count())
        .sum()
}

pub fn solution2(input: &str) -> usize {

    let mut dish_indices: Vec<Rc<ReflectorDish>> = Vec::new();
    let mut seen_dishes: HashMap<Rc<ReflectorDish>, usize> = HashMap::new();

    let dish: ReflectorDish = input.parse().unwrap();
    let mut dish = Rc::new(dish);


    let mut last_index = 0;

    let upper_bound = 1000000000;

    for i in 0..upper_bound {
        if cfg!(any(test, feature = "debug")) {
            println!("after {} full turns\n{dish}", i);
        }
        last_index = i;

        if seen_dishes.contains_key(&dish) {
            break;
        } else {
            dish_indices.push(dish.clone());
            seen_dishes.insert(dish.clone(), i);
        }

        let mut new_dish = (*dish).clone();

        for dir in [Direction::North, Direction::West, Direction::South, Direction::East] {
            new_dish = new_dish.shift(dir);
        }


        dish = Rc::new(new_dish);

    }
    
    if cfg!(any(test, feature = "debug")) {
        println!("{:?}/{}", seen_dishes.get(&dish), last_index);
    }

    let prelude = seen_dishes[&dish];
    let repeated = last_index - seen_dishes[&dish];

    let offset = upper_bound - prelude;
    let idx = offset % repeated;
    let dish_idx = idx + prelude;

    let my_dish = &dish_indices[dish_idx];

    if cfg!(any(test, feature = "debug")) {
        println!("prelude: {prelude} repeated: {repeated} offset: {offset} idx: {idx} dish_idx: {dish_idx}");
    }

    if cfg!(any(test, feature = "pretty")) {
        println!("dish {dish_idx}:\n{}", my_dish);
    }

    my_dish.calc_load()
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day14/example.txt");

    #[test]
    fn test_s1() {
        let s = solution1(INPUT);
        assert_eq!(s, 136);
    }

    #[test]
    fn test_s2() {
        let s = solution2(INPUT);
        assert_eq!(s, 64)
    }

}