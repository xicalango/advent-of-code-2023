use std::collections::HashMap;
use std::fmt::Debug;
use std::time::{Duration, Instant};

use crate::solution::{SolutionReporter, SolutionResult, Solution, SolutionId};

#[derive(Debug, Default)]
struct BenchReporter {
    preprocess_total: Option<Duration>,
    total: Duration,
}

impl SolutionReporter for BenchReporter {

    fn preprocess<R, F: FnOnce() -> R>(&mut self, f: F) -> R {
        let start = Instant::now();
        let r = f();
        let elapsed = start.elapsed();

        *self.preprocess_total.get_or_insert_with(Default::default) += elapsed;

        println!("preprocess -- {elapsed:?}");

        r
    }

    fn run<R: Into<SolutionResult>, F: FnOnce() -> R>(&mut self, f: F) {
        let start = Instant::now();
        let s: SolutionResult = f().into();
        let elapsed = start.elapsed();
        self.total += elapsed;

        match s {
            SolutionResult::One(s) => {
                println!("{s} -- {elapsed:?}");
            },
            SolutionResult::Combined((s1, s2)) => {
                println!("{s1}");
                println!("{s2}");
            },
        };
    }

}

impl BenchReporter {

    fn grand_total(&self) -> Duration {
        self.preprocess_total.unwrap_or(Duration::default()) + self.total
    }

}

#[derive(Debug)]
pub struct Bench {
    times: HashMap<usize, Duration>,
}

impl Bench {
    pub fn new() -> Bench {
        Bench {
            times: HashMap::new(),
        }
    }

    pub fn run<S: Solution + SolutionId>(&mut self) {
        let name = format!("Day {}", S::id());
        println!("{name}");

        let mut reporter = BenchReporter::default();

        S::main(&mut reporter);

        let suffix = reporter.preprocess_total.map(|pp| format!(" (preprocess: {pp:?} execution: {:?})", reporter.total)).unwrap_or_default();
        println!("{name} took {:?}{}", reporter.grand_total(), suffix);
        println!();

        self.times.insert(S::id(), reporter.grand_total());

    }

    pub fn print_times(&self) {
        let mut keys: Vec<&usize> = self.times.keys().collect();
        keys.sort();

        for key in keys {
            println!("Day {}: {:?}", key, self.times[key]);
        }
    }

    pub fn print_slowest_days<const N: usize>(&self) {
        let mut values: Vec<(&usize, &Duration)> = self.times.iter().collect();
        values.sort_by(|(_, d1), (_, d2)| d2.cmp(d1));

        println!("{} slowest days:", N);
        for (day, dur) in values.iter().take(N) {
            println!("Day {}: {:?}", day, dur);
        }
    }

    pub fn print_total_time(&self) {
        let total_time: Duration = self.times.values().sum();
        println!("Total time elapsed: {:?}", total_time);
    }
}

