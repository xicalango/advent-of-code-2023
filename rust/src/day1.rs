
pub fn solution1(data: &str) -> usize {

    let mut sum = 0;

    for line in data.lines() {
        let line = line.trim_end();
        
        let mut first_digit = None;
        let mut last_digit = None;

        let mut forward_chars = line.chars();
        let mut backward_chars = line.chars().rev();

        while first_digit.is_none() || last_digit.is_none() {
            if let (d @ None, Some(c)) = (&mut first_digit, forward_chars.next()) {
                if c.is_digit(10) {
                    d.replace(c.to_digit(10).unwrap() as usize);
                }
            }
            if let (d @ None, Some(c)) = (&mut last_digit, backward_chars.next()) {
                if c.is_digit(10) {
                    d.replace(c.to_digit(10).unwrap() as usize);
                }
            }
        }

        let first_digit = first_digit.unwrap();
        let last_digit = last_digit.unwrap();

        let number = first_digit * 10 + last_digit;

        sum += number;
    }

    sum
}

pub fn solution2(data: &str) -> usize {
    let mut sum = 0;

    let numbers = vec![
        ("1", 1),
        ("2", 2),
        ("3", 3),
        ("4", 4),
        ("5", 5),
        ("6", 6),
        ("7", 7),
        ("8", 8),
        ("9", 9),
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9),
    ];

    for line in data.lines() {
        let line = line.trim_end();

        let mut digits: Vec<usize> = vec![];

        for i in 0..line.len() {
            let sub_string = &line[i..];
            for (matcher, number) in numbers.iter() {
                if sub_string.starts_with(matcher) {
                    digits.push(*number as usize);
                    break;
                }
            }
        }


        let first_digit = digits.first().unwrap();
        let last_digit = digits.last().unwrap();

        let number = first_digit * 10 + last_digit;

        sum += number;
    }

    sum
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test() {
        let data = include_str!("../../data/day1/example.txt");
        let solution = solution1(data);
        println!("{solution}");
    }

    #[test]
    fn test_2() {
        let data = include_str!("../../data/day1/example2.txt");
        let solution = solution2(data);
        println!("{solution}");

    }

    #[test]
    fn test_nine_two_nine() {
        solution2("9twonine");
    }

    #[test]
    fn test_twone() {
        solution2("twone");
    }
}
