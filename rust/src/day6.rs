
fn calc_distances_lazy(start_time: usize, end_time: usize, total_time: usize) -> impl Iterator<Item=usize> {
    (start_time..=end_time).map(move |charge_time| {
        let speed = charge_time;
        let drive_time = total_time - charge_time;
        let distance = drive_time * speed;
        distance
    })
}

fn calc_distances(time: usize) -> Vec<usize> {
    calc_distances_lazy(0, time, time).collect()
}

pub fn solution1(input: &str) -> usize {
    let mut lines = input.lines();
    let times: Vec<usize> = lines.next().unwrap()
            .split_ascii_whitespace()
            .skip(1)
            .map(|v| v.parse().unwrap())
            .collect();
    let distances: Vec<usize> = lines.next().unwrap()
            .split_ascii_whitespace()
            .skip(1)
            .map(|v| v.parse().unwrap())
            .collect();

    times.into_iter()
            .map(calc_distances)
            .zip(distances)
            .map(|(distances, min_dist)| {
                distances.into_iter().filter(|t| t > &min_dist).count()
            }).product()
}

fn parse_time_distance(input: &str) -> (usize, usize) {
    let mut lines = input.lines();

    let time: usize = lines.next().unwrap()
            .split_ascii_whitespace()
            .skip(1)
            .fold(String::new(), |s, v| s+v)
            .parse().unwrap();
    let distance: usize = lines.next().unwrap()
            .split_ascii_whitespace()
            .skip(1)
            .fold(String::new(), |s, v| s+v)
            .parse().unwrap();

    (time, distance)
}

#[allow(dead_code)]
pub fn solution2(input: &str) -> usize {
    let (time, distance) = parse_time_distance(input);
    calc_distances(time).into_iter().filter(|d| d>&distance).count()
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day6/example.txt");

    #[test]
    fn test_s1() {
        let s1 = solution1(INPUT);
        println!("{s1}");
        assert_eq!(288, s1);
    }

    #[test]
    fn test_s2() {
        let s2 = solution2(INPUT);
        println!("{s2}");
        assert_eq!(71503, s2);
    }

}
