use std::{str::FromStr, collections::HashMap, fmt::Display, io::BufWriter};

use anyhow::{Error, anyhow};

use crate::utils::{direction::{Direction, Walkable}, coor::ICoor, grid::{Grid2DAccess, IGrid2DAccessMut, OffsetGrid, Grid2D, IGrid2DAccess, XpmDisplay}, self};

fn hex_str_to_byte(hex: &str) -> u8 {
    hex.chars().rev().enumerate()
            .fold(0u8, |a, (m, c)| a + c.to_digit(16).map(|v| v as u8 * 16u8.pow(m as u32)).unwrap())
}

#[derive(Debug)]
struct Instruction {
    direction: Direction,
    steps: usize,
}

impl FromStr for Instruction {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.splitn(3, ' ').collect();
        if parts.len() != 3 {
            return Err(anyhow!("invalid: {s}"));
        }

        let dir = match parts.get(0) {
            Some(&"U") => Ok(Direction::North),
            Some(&"R") => Ok(Direction::East),
            Some(&"D") => Ok(Direction::South),
            Some(&"L") => Ok(Direction::West),
            o => Err(anyhow!("invalid: {o:?}")),
        }?;

        let steps: usize = parts.get(1)
                                .ok_or(anyhow!("invalid: {s}"))
                                .and_then(|v| v.parse()
                                    .map_err(|e| anyhow!("invalid {e:?}"))
                                )?;

        Ok(Instruction { 
            direction: dir,
            steps,
        })
    }
}

#[derive(Debug)]
struct HexInstruction<'a>(&'a str);

impl TryFrom<HexInstruction<'_>> for Instruction {
    type Error = Error;

    fn try_from(value: HexInstruction) -> Result<Self, Self::Error> {
        let HexInstruction(value) = value;

        let parts: Vec<&str> = value.splitn(3, ' ').collect();
        if parts.len() != 3 {
            return Err(anyhow!("invalid: {value}"));
        }

        let value = &parts[2][2..8];
        

        let b1 = hex_str_to_byte(&value[0..1]);
        let b2 = hex_str_to_byte(&value[1..3]);
        let b3 = hex_str_to_byte(&value[3..5]);
        let b4 = hex_str_to_byte(&value[5..6]);

        let p1 = 256usize * 256usize * b1 as usize;
        let p2 = 256usize * b2 as usize;
        let p3 = b3 as usize;

        let dist = p1 + p2 + p3;

        let dir = match b4 {
            0 => Direction::East,
            1 => Direction::South,
            2 => Direction::West,
            3 => Direction::North, 
            o => return Err(anyhow!("illegal {o}")),
        };

        Ok(Instruction { direction: dir, steps: dist })
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
enum Tile {
    Trench,
    Ground,
}

impl Display for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let c = match self {
            Tile::Trench => '#',
            Tile::Ground => '.',
        };
        write!(f, "{c}")
    }
}

impl Tile {

    fn is_tile(&self) -> bool {
        match self {
            Tile::Trench => true,
            Tile::Ground => false,
        }
    }

    fn is_ground(&self) -> bool {
        !self.is_tile()
    }

}

impl XpmDisplay for Tile {
    fn character_colors() -> Vec<(char, String)> {
        vec![
            ('#', "#FFFFFF".to_string()),
            ('.', "#000000".to_string())
        ]
    }

    fn xpm_char(&self) -> char {
        match self {
            Tile::Trench => '#',
            Tile::Ground => '.',
        }
    }
}


#[derive(Debug)]
struct Turtle<'a, G: IGrid2DAccessMut<Item = Tile>> {
    grid: &'a mut G,
    pos: ICoor,
}

impl<'a, G: IGrid2DAccessMut<Item = Tile>> Turtle<'a, G> {

    fn new(map: &'a mut G) -> Turtle<G> {
        Turtle::new_at(map, (0,0))
    }

    fn new_at(map: &'a mut G, pos: ICoor) -> Turtle<G> {
        Turtle { grid: map, pos }
    }

    fn execute(&mut self, instr: &Instruction) {
        for _ in 0..instr.steps {
            let new_pos @ (x, y) = self.pos.walk(&instr.direction);
            self.grid.replace_i(x, y, Tile::Trench);
            self.pos = new_pos;
        }
    }

}

struct PosTurtle<'a, I: Iterator<Item = &'a Instruction>> {
    instructions: I,
    start: bool,
    pos: ICoor,
}

impl<'a, I: Iterator<Item = &'a Instruction>> Iterator for PosTurtle<'a, I> {
    type Item = ICoor;

    fn next(&mut self) -> Option<Self::Item> {
        if self.start {
            self.start = false;
            Some(self.pos)
        } else {
            self.instructions.next().map(|instr| {
                let (dx, dy): ICoor = instr.direction.into();
                let (dx, dy) = (dx * instr.steps as isize, dy * instr.steps as isize);
                let (x, y) = self.pos;
                let next = (x + dx, y + dy);
                self.pos = next;
                next
            })
        }
    }
}

impl<'a, I: Iterator<Item = &'a Instruction>> PosTurtle<'a, I> {

    fn new(instructions: I) -> PosTurtle<'a, I> {
        PosTurtle { instructions, start: true, pos: (0, 0) }
    }

}

#[derive(Debug)]
struct Map {
    grid: OffsetGrid<Grid2D<Tile>>,
}

impl Map {

    fn print_xpm(&self) {
        use std::fs::File;
        use std::io::Write;
        let f = File::create(option_env!("XPM_OUTPUT").unwrap_or("day18_map.xpm")).unwrap();
        let mut w = BufWriter::new(f);
        write!(w, "{}", self.grid.xpm_display()).unwrap();
    }

    #[cfg(feature = "day18_old")]
    fn fill_mid(&mut self) {
        for y in 0..self.grid.height() {
            let mut inside = false;
            let mut last_border = false;
            for x in 0..self.grid.width() {
                match self.grid.get(x, y) {
                    Some(Tile::Trench) => {
                        if !last_border {
                            inside = !inside;
                        }
                        last_border = true;
                    },
                    None | Some(Tile::Ground) => {
                        if inside {
                            self.grid.replace(x, y, Tile::Trench);
                        }
                        last_border = false;
                    },
                }
            }
        }
    }

    #[cfg(not(feature = "day18_old"))]
    fn fill_mid(&mut self) {
        use crate::utils::{graph::bfs::{Bfs, CallbackResult}, grid::Grid2DAccessMut};

        let (w, h) = utils::grid::Grid2DAccess::dimension(&self.grid);
        let (mx, mh) = (w/2, h/2);

        let bfs = Bfs::new(&mut self.grid);

        bfs.bfs_filtered(&(mx, mh), 
            |g, id| !g.get_coor(id).is_some_and(|t| t.is_tile()), 
            |g, cur, _, _| {
            g.replace_coor(cur, Tile::Trench);
            CallbackResult::Continue
        });

    }

}

pub fn solution1(input: &str) -> usize {
    
    let mut grid: HashMap<ICoor, Tile> = HashMap::new();
    let mut turtle = Turtle::new(&mut grid);

    input.lines().map(|l| l.parse::<Instruction>().unwrap()).for_each(|v| {
        turtle.execute(&v);
    });

    let (ox, oy) = grid.origin();
    let (w, h) = grid.dimension();

    #[cfg(any(test, debug = "feature"))]
    println!("{ox}, {oy}, {w}, {h}");

    let ugrid: Grid2D<Tile> = Grid2D::new_with(w + 1, h + 1, |x, y| {
        let ix = ox.checked_add_unsigned(x).unwrap();
        let iy = oy.checked_add_unsigned(y).unwrap();
        grid.get(&(ix, iy)).unwrap_or(&Tile::Ground).to_owned()
    });

    let offset_grid = OffsetGrid::new((ox, oy), ugrid);

    let mut map = Map { grid: offset_grid };
    #[cfg(any(test, feature = "debug"))]
    println!("{}", map.grid.display());

    map.fill_mid();

    #[cfg(feature = "pretty")]
    map.print_xpm();

    map.grid.all_elements().filter(|c| c.is_tile()).count()
}

pub fn solution2(input: &str) -> usize {
    let instr: Vec<Instruction> = input.lines().map(|l| Instruction::try_from(HexInstruction(l)).unwrap()).collect();
    let post = PosTurtle::new(instr.iter());

    let pos: Vec<_> = post.collect();

    let mut area2 = 0isize;

    for coor in pos.windows(2) {
        let start = &coor[0];
        let end = &coor[1];

        let det = (start.0 * end.1) - (end.0 * start.1);
        area2 += det;
    }

    let area = (area2 / 2) as usize;
    let boundary: usize = instr.into_iter().map(|i| i.steps).sum();
    area + boundary/2 + 1
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day18/example.txt");

    #[test]
    fn test_hex() {
        let line = "R 6 (#70c710)";
        let instr: Instruction = line.parse().unwrap();
        println!("{instr:?}");
    }

    #[test]
    fn test_s1() {
        let s = solution1(INPUT);
        assert_eq!(s, 62);
    } 

    #[test]
    fn test_s2() {
        let s = solution2(INPUT);
        assert_eq!(s, 952408144115);
    } 

}
