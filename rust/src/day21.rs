use std::collections::{VecDeque, HashSet};

use crate::utils::{grid::{Grid2D, Grid2DAccess, InfiniGrid, Grid2DAccessMut, IGrid2DAccess}, direction::{Direction, TryWalkable}};

pub fn solution1<const N_STEPS: usize>(input: &str) -> usize {

    let grid: Vec<Vec<_>> = input.lines().map(|l| l.chars().collect()).collect();
    let mut grid: Grid2D<_> = grid.into();

    let start = grid.find(|c| c == &'S').unwrap();
    grid.replace_coor(&start, '.');

    let grid = grid;

    let start = (start.0 as isize, start.1 as isize);

    let infini_grid = InfiniGrid::new(grid);

    let mut visited = HashSet::new();

    let mut visit_queue: VecDeque<_> = vec![(start, 0)].into();

    let mut counter = 0;

    let mut even_parity_visits = HashSet::new();

    while let Some((pos, steps)) = visit_queue.pop_front() {
        if steps > N_STEPS {
            break;
        }

        if visited.contains(&pos) {
            continue;
        }

        if steps % 2 == 0 {
            even_parity_visits.insert(pos.clone());
            counter += 1;
        }

        let destinations: Vec<_> = Direction::all()
                .iter()
                .filter_map(|dir| pos.try_walk(dir))
                .filter(|c| infini_grid.get_icoor(c).is_some_and(|t| t == &'.'))
                .collect();

        for dest in destinations.iter() {
            visit_queue.push_back((dest.clone(), steps + 1))
        }

        visited.insert(pos);
    }

    if cfg!(any(feature = "debug")) {
        for y in -20..30 {
            for x in -20..30 {
                if (x, y) == start {
                    print!("S");
                } else if even_parity_visits.contains(&(x, y)) {
                    print!("O");
                } else {
                    match infini_grid.get_i(x, y) {
                        Some(c) => print!("{c}"),
                        None => print!(" "),
                    }
                }
            }
            println!();
        }
    }

    counter
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day21/example.txt");

    #[test]
    fn test_s1() {
        let s = solution1::<6>(INPUT);
        assert_eq!(s, 16)
    }

    #[test]
    fn test_s2_1() {
        assert_eq!(solution1::<10>(INPUT), 50);
        assert_eq!(solution1::<50>(INPUT), 1594);
        assert_eq!(solution1::<100>(INPUT), 6536);
        assert_eq!(solution1::<500>(INPUT), 167004);
        // assert_eq!(solution1::<1000>(INPUT), 668697);
        // assert_eq!(solution1::<5000>(INPUT), 16733044);
    }
}
