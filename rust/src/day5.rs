use std::{str::FromStr, ops::RangeInclusive};
use anyhow::{Result, Error, anyhow};

use crate::utils::ranges::{RangeExt, RangeLength, SplitOff};

type Range = RangeInclusive<usize>;

#[derive(Debug)]
pub struct RangeMapping {
    src_start: usize,
    dst_start: usize,
    len: usize,
}

impl FromStr for RangeMapping {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Result<Vec<usize>> = s.split(' ')
                .map(|p| p.trim().parse::<usize>().map_err(|e| anyhow!("invalid number {e:?}")))
                .collect();
        let parts = parts?;
        if parts.len() != 3 {
            return Err(anyhow!("invalid range mapping: {s}"));
        }

        Ok(RangeMapping {
            src_start: parts[1],
            dst_start: parts[0],
            len: parts[2]
        })
    }
}

impl RangeMapping {

    pub fn contains_src(&self, value: usize) -> bool {
        value >= self.src_start && value <= self.src_start + self.len - 1
    }

    pub fn src_range(&self) -> Range {
        self.src_start..=self.src_start + self.len - 1
    }

    pub fn map(&self, value: usize) -> Option<usize> {
        if !self.contains_src(value) {
            return None;
        }

        let diff = value - self.src_start;
        Some(self.dst_start + diff)
    }

    pub fn split_sr(&self, range: &Range) -> SplitOff<Range> {
        range.split_off(&self.src_range())
    }

}

#[derive(Debug, Default)]
pub struct Mapping {
    range_mappings: Vec<RangeMapping>,
}

impl From<Vec<RangeMapping>> for Mapping {
    fn from(value: Vec<RangeMapping>) -> Self {
        Mapping { range_mappings: value }
    }
}

impl Mapping {

    pub fn map(&self, value: usize) -> usize {
        for mapping in self.range_mappings.iter() {
            if let Some(mapped) = mapping.map(value) {
                return mapped;
            }
        }
        return value;
    }

    pub fn split_one_range(&self, range: &Range) -> Vec<Range> {
        for mapping in self.range_mappings.iter() {
            match mapping.split_sr(range) {
                SplitOff::TwoSplit(s1, s2) => {
                    let r1 = self.split_one_range(&s1);
                    let r2 = self.split_one_range(&s2);

                    return [r1, r2].concat();
                },
                SplitOff::ThreeSplit(s1, s2, s3) => {
                    let r1 = self.split_one_range(&s1);
                    let r2 = self.split_one_range(&s3);

                    return [r1, vec![s2], r2].concat();
                },
                SplitOff::SplitContainsFully(s) => {
                    return vec![s];
                },
                SplitOff::NoOverlap(_) => {
                },
            };
        }

        return vec![range.clone()];
    }

    pub fn split_ranges(&self, ranges: &Vec<Range>) -> Vec<Range> {
        ranges.iter().flat_map(|r| self.split_one_range(r).into_iter()).collect()
    }

    pub fn map_ranges_unchecked(&self, ranges: &Vec<Range>) -> Vec<Range> {
        let mut result = Vec::new();

        for range in ranges {
            let start = range.start();
            let len = range.len();
            let mapped_start = self.map(*start);
            result.push(mapped_start..=(mapped_start+len-1));
        }

        result
    }

}

pub fn solution1(input: &str) -> Result<usize> {

    let mut lines = input.lines();
    let seeds = lines.next().ok_or(anyhow!("no seeds"))?;
    let (_, seeds) = seeds.split_once(':').ok_or(anyhow!("invalid seeds"))?;

    let values: Result<Vec<usize>> = seeds.split_ascii_whitespace().map(|v| v.parse().map_err(|e| anyhow!("invalid line {v}:{e:?}"))).collect();
    let mut values = values?;

    let mut mapping = Mapping::default();

    for line in lines.skip(1) {
        let line = line.trim_end();
        if line.is_empty() {
            values = values.into_iter().map(|v| mapping.map(v)).collect();
            mapping.range_mappings.clear();
        } else if line.contains(":") {
            //skip
        } else {
            let range_mapping: RangeMapping = line.parse()?;
            mapping.range_mappings.push(range_mapping);
        }
    }

    Ok(values.into_iter().min().ok_or(anyhow!("no solution?"))?)
}

pub fn solution2(input: &str) -> Result<usize> {
    let mut lines = input.lines();
    let seeds = lines.next().ok_or(anyhow!("no seeds"))?;
    let (_, seeds) = seeds.split_once(':').ok_or(anyhow!("invalid seeds"))?;

    let values: Result<Vec<usize>> = seeds.split_ascii_whitespace().map(|v| v.parse().map_err(|e| anyhow!("invalid line {v}:{e:?}"))).collect();
    let mut ranges: Vec<Range> = values?.chunks(2).map(|c| c[0]..=c[0]+c[1]-1).collect();

    let mut mapping = Mapping::default();

    for line in lines.skip(1) {
        let line = line.trim_end();
        if line.is_empty() {
            let split_ranges = mapping.split_ranges(&ranges);
            ranges = mapping.map_ranges_unchecked(&split_ranges);
            mapping.range_mappings.clear();
        } else if line.contains(":") {
            //skip
        } else {
            let range_mapping: RangeMapping = line.parse()?;
            mapping.range_mappings.push(range_mapping);
        }
    }

    Ok(ranges.into_iter().map(|r| *r.start()).min().ok_or(anyhow!("no solution??"))?)
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day5/example.txt");

    #[test]
    fn test_s1() {
        let s1 = solution1(INPUT);
        println!("{s1:?}");
        assert_eq!(Some(35), s1.ok());
    }

    #[test]
    fn test_split() {
        let range1 = 79..=(79+13);
        let range2 = 55..=(55+12);

        let ranges = vec![range1, range2];

        let range_mapping1: RangeMapping = "50 98 2".parse().unwrap();
        let range_mapping2: RangeMapping = "52 50 48".parse().unwrap();

        println!("{:?}, {:?}", range_mapping1.src_range(), range_mapping2.src_range());

        let range_mappings: Mapping = vec![range_mapping1, range_mapping2].into();

        let split = range_mappings.split_ranges(&ranges);
        println!("{ranges:?} -> {split:?}");
        let mapped = range_mappings.map_ranges_unchecked(&split);
        println!("{split:?} -> {mapped:?}");
    }

    #[test]
    fn test_s2() {
        let s = solution2(INPUT);
        println!("{s:?}");
        assert_eq!(Some(46), s.ok());
    }

    #[test]
    fn test_fail1() {
        let ranges: Vec<Range> = vec![2982244904..=3320008701];

        let mapping_str = "3566547172 3725495029 569472267
2346761246 1249510998 267846697
1812605508 937956667 271194541
1421378697 1209151208 40359790
2083800049 2788751092 262961197
2938601691 473979048 463977619
473979048 1517357695 947399649
4136019439 3566547172 158947857
1461738487 3051712289 350867021
2614607943 2464757344 323993748";

        let rms: Vec<RangeMapping> = mapping_str.lines().map(|l| l.trim().parse().unwrap()).collect();

        let mapping: Mapping = rms.into();

        let split = mapping.split_one_range(&ranges[0]);
        println!("{split:?}");
    }

}
