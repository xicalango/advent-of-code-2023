
fn num_diff(s1: &str, s2: &str) -> usize {
    s1.chars().zip(s2.chars()).map(|(a, b)| (a != b) as usize).sum()
}

fn find_reflection<const SMUDGE: bool>(values: &[&str]) -> Option<usize> {

    for i in 0..values.len()-1 {
        let mut smudge_available = SMUDGE;
        for s in 0..=i as isize {
            if s as usize > i {
                break;
            }
            let i1 = i.saturating_add_signed(-s);
            let i2 = i.saturating_add_signed(s) + 1;
            if i2 >= values.len() {
                break;
            }
            let v1 = values[i1];
            let v2 = values[i2];
            
            let diffs = num_diff(v1, v2);

            if cfg!(any(test, feature = "debug")) {
                println!("{i}:{s} -- {v1} == {v2} -> {diffs}");
            }

            if diffs == 1 && smudge_available {
                smudge_available = false;
            } else if diffs > 0 {
                break;
            }

            if (i1 == 0 || i2 == values.len() - 1) && !smudge_available {
                if cfg!(any(test, feature = "debug")) {
                    println!("reflection at {} {SMUDGE}/{smudge_available}", i);
                }
                
                return Some(i + 1);
            }
        }
    }

    None
}

pub fn solution<const SMUDGE: bool>(input: &str) -> usize {

    let mut rows = Vec::new();
    let mut cols: Vec<String> = Vec::new();

    let mut result = 0;

    for (line_num, line) in input.lines().map(|l| l.trim_end()).enumerate() {
        if line.is_empty() {

            let ccols: Vec<&str> = cols.iter().map(|v| v.as_str()).collect();

            if let Some(r) = find_reflection::<SMUDGE>(&rows) {
                if cfg!(feature = "pretty") {
                    println!("{line_num} {r} R")
                }
                result += r * 100;
            } else if let Some(r) = find_reflection::<SMUDGE>(&ccols) {
                if cfg!(feature = "pretty") {
                    println!("{line_num} {r} C")
                }
                result += r;
            } else {
                panic!("invalid @{line_num}");
            }

            cols.clear();
            rows.clear();

            if cfg!(any(test, feature = "debug")) {
                println!();
            }

        } else {
            if cols.len() < line.len() {
                cols.resize_with(line.len(), String::new);
            }

            for (c, s) in line.chars().zip(cols.iter_mut()) {
                s.push(c);
            }

            rows.push(line);
        }
    }

    result
}

pub fn solution1(input: &str) -> usize {
    solution::<false>(input)
}

pub fn solution2(input: &str) -> usize {
    solution::<true>(input)
}

#[cfg(test)]
mod test {

    use super::*;

    const INPUT: &'static str = include_str!("../../data/day13/example.txt");

    #[test]
    fn test_s1() {
        let s = solution1(INPUT);
        assert_eq!(s, 405);
    }

    #[test]
    fn test_s2() {
        let s = solution2(INPUT);
        assert_eq!(s, 400);
    }

}

